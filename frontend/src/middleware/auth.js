import api from "../services/api";
import store from "@/store";

export default function auth({ router, next }) {
  var session = localStorage.getItem("session");
  if (session == null) {
    return router.push({ name: "Login" });
  }
  session = JSON.parse(session);
  api.defaults.headers.common["Authorization"] = "JWT " + session.token;
  store.dispatch("refreshMyPermList");
  next();
}
