import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import ListarProyecto from "../views/proyectos/ListarProyecto.vue";
import ObtenerProyecto from "../views/proyectos/ObtenerProyecto.vue";
import EditarProyecto from "../views/proyectos/EditarProyecto.vue";
import ListarFase from "../views/fases/ListarFase.vue";
import ObtenerFase from "../views/fases/ObtenerFase.vue";
import ObtenerTipoDeItem from "../views/tipoDeItem/ObtenerTipoDeItem.vue";
import ListarUsuarios from "../views/usuarios/ListarUsuarios.vue";
import ObtenerUsuarios from "../views/usuarios/ObtenerUsuarios.vue";
import ListarRol from "../views/roles/ListarRol.vue";
import ObtenerRol from "../views/roles/ObtenerRol.vue";
import EditarRol from "../views/roles/EditarRol.vue";
import ObtenerItem from "../views/item/ObtenerItem.vue";
import EditarItem from "../views/item/editarItem.vue";
import ObtenerVersion from "../views/item/ObtenerVersion";
import ObtenerSolicitud from "../views/solicitud/ObtenerSolicitud";
import auth from "../middleware/auth";
import guest from "../middleware/guest";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      middleware: guest
    }
  },
  {
    path: "/proyectos",
    name: "Proyectos",
    component: ListarProyecto,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/proyectos/:id",
    name: "Obtener Proyectos",
    component: ObtenerProyecto,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/proyectos/:id/edit",
    name: "Editar Proyectos",
    component: EditarProyecto,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/fases",
    name: "Fases",
    component: ListarFase,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/fases/:id",
    name: "Obtener Fases",
    component: ObtenerFase,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/tipo-items/:id",
    name: "Tipo de Items",
    component: ObtenerTipoDeItem,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/items/:id",
    name: "Obtener Item",
    component: ObtenerItem,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/items/:id/versiones/:ver",
    name: "Obtener Version",
    component: ObtenerVersion,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/items/:id/edit",
    name: "Editar Item",
    component: EditarItem,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/usuarios",
    name: "Usuarios",
    component: ListarUsuarios,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/usuarios/:id",
    name: "Usuarios",
    component: ObtenerUsuarios,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/roles",
    name: "Roles",
    component: ListarRol,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/roles/:id",
    name: "Roles",
    component: ObtenerRol,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/roles/:id/edit",
    name: "Editar Rol",
    component: EditarRol,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/solicitudes/:id",
    name: "Solicitudes",
    component: ObtenerSolicitud,
    meta: {
      middleware: auth
    }
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  routes
});

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  if (!subsequentMiddleware) return context.next;

  return (...parameters) => {
    context.next(...parameters);
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];
    const context = {
      from,
      next,
      to,
      router
    };
    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({ ...context, next: nextMiddleware });
  }
  return next();
});

export default router;
