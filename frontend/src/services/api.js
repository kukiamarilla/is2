import Vue from "vue";
import axios from "axios";
import router from "@/router";

let api = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 5000,
  headers: {
    "Content-Type": "application/json"
  }
});

api.interceptors.response.use(
  res => res,
  err => {
    if (err.response.data.error == "unauthenticated") {
      localStorage.removeItem("session");
      router.push({ name: "Login" });
    }
    Vue.notify({
      title: "Error",
      text: err.response.data.message,
      type: "error"
    });
    return err;
  }
);

export default api;
