import api from "./api.js";

export default {
  listar() {
    return api.get(`tipo-item/`).then(tipoItem => tipoItem.data);
  },
  obtener(id) {
    return api.get(`tipo-item/${id}`).then(tipoItem => tipoItem.data);
  },
  crear(tipoItem) {
    return api.post(`tipo-item/`, tipoItem).then(tipoItem => tipoItem.data);
  },
  modificar(id, tipoItem) {
    return api.put(`tipo-item/${id}`, tipoItem).then(tipoItem => tipoItem.data);
  },
  eliminar(id) {
    return api.delete(`tipo-item/${id}`).then(result => result.data);
  },
  campos(id) {
    return api.get(`tipo-item/${id}/campos/`).then(campos => campos.data);
  }
};
