import api from "./api.js";

export default {
  listar() {
    return api.get(`proyectos/`).then(proyecto => proyecto.data);
  },
  obtener(id) {
    return api.get(`proyectos/${id}/`).then(proyecto => proyecto.data);
  },
  crear(proyecto) {
    return api.post(`proyectos/`, proyecto).then(proyecto => proyecto.data);
  },
  modificar(id, proyecto) {
    return api
      .put(`proyectos/${id}/`, proyecto)
      .then(proyecto => proyecto.data);
  },
  eliminar(id) {
    return api.delete(`proyectos/${id}/`).then(result => result.data);
  },
  fases(id) {
    return api.get(`proyectos/${id}/fases/`).then(proyecto => proyecto.data);
  },
  miembros(id) {
    return api.get(`proyectos/${id}/miembros/`).then(proyecto => proyecto.data);
  },
  agregarMiembro(id, payload) {
    return api
      .post(`proyectos/${id}/miembros/`, payload)
      .then(proyecto => proyecto.data);
  },
  eliminarMiembro(id, payload) {
    return api
      .delete(`proyectos/${id}/miembros/`, { data: payload })
      .then(proyecto => proyecto.data);
  },
  misPermisos(id) {
    return api
      .get(`proyectos/${id}/mis_permisos/`)
      .then(proyecto => proyecto.data);
  },
  verComite(id) {
    return api.get(`proyectos/${id}/comite/`).then(comite => comite.data);
  },
  crearComite(id, payload) {
    return api
      .post(`proyectos/${id}/comite/`, payload)
      .then(proyecto => proyecto.data);
  },
  iniciar(id) {
    return api.post(`proyectos/${id}/iniciar/`).then(proyecto => proyecto.data);
  },
  solicitarRuptura(payload) {
    return api.post(`solicitud/`, payload).then(proyecto => proyecto.data);
  },
  cancelar(id) {
    return api
      .post(`proyectos/${id}/cancelar/`)
      .then(proyecto => proyecto.data);
  },
  terminar(id) {
    return api
      .post(`proyectos/${id}/terminar/`)
      .then(proyecto => proyecto.data);
  },
  listarSolicitudes(id) {
    return api
      .get(`proyectos/${id}/solicitudes/`)
      .then(solicitud => solicitud.data);
  }
};
