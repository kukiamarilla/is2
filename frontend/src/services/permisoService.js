import api from "./api.js";

export default {
  listar() {
    return api.get(`permisos/`).then(result => result.data);
  }
};
