import api from "./api.js";

export default {
  listar(tipo) {
    return api.get(`roles/?tipo=${tipo}`).then(rol => rol.data);
  },
  obtener(id) {
    return api.get(`roles/${id}/`).then(rol => rol.data);
  },
  crear(rol) {
    return api.post(`roles/`, rol).then(rol => rol.data);
  },
  modificar(id, rol) {
    return api.put(`roles/${id}/`, rol).then(rol => rol.data);
  },
  eliminar(id) {
    return api.delete(`roles/${id}/`).then(result => result.data);
  },
  agregar(id, permission) {
    return api.post(`roles/${id}/permisos/`, permission).then(rol => rol.data);
  },
  remover(id, permission) {
    return api
      .delete(`roles/${id}/permisos/`, { data: permission })
      .then(rol => rol.data);
  },
  permisos(id) {
    return api
      .get(`roles/${id}/permisos/`)
      .then(permissions => permissions.data);
  }
};
