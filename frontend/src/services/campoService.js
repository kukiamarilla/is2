import api from "./api.js";

export default {
  obtener(id) {
    return api.get(`campos/${id}`).then(campo => campo.data);
  },
  crear(campo) {
    return api.post(`campos/`, campo).then(campo => campo.data);
  },
  modificar(id, campo) {
    return api.put(`campos/${id}`, campo).then(campo => campo.data);
  },
  eliminar(id) {
    return api.delete(`campos/${id}`).then(result => result.data);
  }
};
