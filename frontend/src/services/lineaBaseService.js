import api from "./api.js";

export default {
  obtener(lbID) {
    return api.get(`lineas-base/${lbID}`).then(lb => lb.data);
  },
  crear(payload) {
    return api.post(`lineas-base/`, payload).then(lb => lb.data);
  }
};
