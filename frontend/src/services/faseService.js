import api from "./api.js";

export default {
  listar() {
    return api.get(`fases`).then(fase => fase.data);
  },
  obtener(id) {
    return api.get(`fases/${id}`).then(fase => fase.data);
  },
  crear(fase) {
    return api.post(`fases/`, fase).then(fase => fase.data);
  },
  modificar(id, fase) {
    return api.put(`fases/${id}`, fase).then(fase => fase.data);
  },
  eliminar(id) {
    return api.delete(`fases/${id}`).then(result => result.data);
  },
  intercambiar(src, dest) {
    return api
      .put(`fases/${src}/intercambiar/`, { id: dest })
      .then(result => result.data);
  },
  miembros(id) {
    return api.get(`fases/${id}/miembros/`).then(fase => fase.data);
  },
  agregarMiembro(id, payload) {
    return api.post(`fases/${id}/miembros/`, payload).then(fase => fase.data);
  },
  eliminarMiembro(id, payload) {
    return api.delete(`fases/${id}/miembros/`, payload).then(fase => fase.data);
  },
  misPermisos(id) {
    return api.get(`fases/${id}/mis_permisos/`).then(permisos => permisos.data);
  },
  tipoItems(id) {
    return api.get(`fases/${id}/tipo_items/`).then(tipos => tipos.data);
  },
  listarPosiblesPadresParaItem(faseID) {
    return api
      .get(`fases/${faseID}/posibles_padres/`)
      .then(items => items.data);
  },
  listarItems(faseID) {
    return api.get(`fases/${faseID}/items/`).then(items => items.data);
  },
  listarLB(faseID) {
    return api.get(`fases/${faseID}/lineas_base/`).then(lb => lb.data);
  },
  importar(id, payload) {
    return api.post(`fases/${id}/importar/`, payload).then(tipo => tipo.data);
  },
  cerrar(id) {
    return api.post(`fases/${id}/cerrar/`).then(fase => fase.data);
  }
};
