import api from "./api.js";

export default {
  listar() {
    return api.get(`usuarios/`).then(usuario => usuario.data);
  },
  obtener(id) {
    return api.get(`usuarios/${id}`).then(usuario => usuario.data);
  },
  activar(id) {
    return api.post(`usuarios/${id}/activar/`).then(usuario => usuario.data);
  },
  desactivar(id) {
    return api.post(`usuarios/${id}/desactivar/`).then(usuario => usuario.data);
  },
  roles(id) {
    return api.get(`usuarios/${id}/roles/`).then(usuario => usuario.data);
  },
  agregarRol(id, payload) {
    return api
      .post(`usuarios/${id}/roles/`, payload)
      .then(usuario => usuario.data);
  },
  eliminarRol(id, payload) {
    return api
      .delete(`usuarios/${id}/roles/`, { data: payload })
      .then(usuario => usuario.data);
  },
  misPermisos() {
    return api.get(`usuarios/mis_permisos/`).then(permisos => permisos.data);
  },
  me() {
    return api.get(`usuarios/me/`).then(usuario => usuario.data);
  }
};
