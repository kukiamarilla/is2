import api from "./api.js";

export default {
  obtener(itemID) {
    return api.get(`items/${itemID}`).then(item => item.data);
  },
  crear(formData) {
    return api.post(`items/`, formData).then(item => item.data);
  },
  modificar(itemID, formData) {
    return api.put(`items/${itemID}`, formData).then(item => item.data);
  },
  actualizarVersion(itemID, formData) {
    return api.put(`items/${itemID}/`, formData).then(item => item.data);
  },
  revertirVersion(itemID, payload) {
    return api
      .post(`items/${itemID}/revertir/`, payload)
      .then(item => item.data);
  },
  listarPosiblesPadresParaModificar(itemID) {
    return api
      .get(`items/${itemID}/posibles_padres/`)
      .then(items => items.data);
  },
  solicitarAprobacion(itemID) {
    return api
      .post(`items/${itemID}/solicitar_aprobacion/`)
      .then(item => item.data);
  },
  aprobar(itemID) {
    return api.post(`items/${itemID}/aprobar/`).then(item => item.data);
  },
  solicitarDesaprobacion(itemID, motivo) {
    return api
      .post(`items/${itemID}/solicitar_desaprobacion/`, { motivo: motivo })
      .then(item => item.data);
  },
  desaprobar(itemID) {
    return api.post(`items/${itemID}/desaprobar/`).then(item => item.data);
  },
  desactivar(itemID) {
    return api.post(`items/${itemID}/desactivar/`).then(item => item.data);
  }
};
