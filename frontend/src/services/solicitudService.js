import api from "./api.js";

export default {
  obtener(id) {
    return api.get(`solicitud/${id}`).then(solicitud => solicitud.data);
  },
  votar(id, payload) {
    return api
      .post(`solicitud/${id}/votar/`, payload)
      .then(solicitud => solicitud.data);
  }
};
