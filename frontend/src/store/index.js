import Vue from "vue";
import Vuex from "vuex";
import UsuarioService from "@/services/usuarioService";
import ProyectoService from "@/services/proyectoService";
import FaseService from "@/services/faseService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    session: {},
    myPerms: [],
    myProjectPerms: [],
    myPhasePerms: []
  },
  getters: {
    hasPerm: state => perm => {
      return state.myPerms.indexOf(perm) > -1;
    },
    hasProjectPerm: state => perm => {
      return state.myProjectPerms.indexOf(perm) > -1;
    },
    hasPhasePerm: state => perm => {
      return state.myPhasePerms.indexOf(perm) > -1;
    }
  },
  mutations: {
    setMyPerms(state, perms) {
      state.myPerms = perms;
    },
    setMyProjectPerms(state, perms) {
      state.myProjectPerms = perms;
    },
    setMyPhasePerms(state, perms) {
      state.myPhasePerms = perms;
    }
  },
  actions: {
    refreshMyPermList({ commit }) {
      UsuarioService.misPermisos().then(perms => {
        commit("setMyPerms", perms);
      });
    },
    removeMyPerms({ commit }) {
      commit("setMyPerms", []);
    },
    loadMyProjectPerms({ commit }, projectID) {
      ProyectoService.misPermisos(projectID).then(perms => {
        commit("setMyProjectPerms", perms);
      });
    },
    loadMyPhasePerms({ commit }, phaseID) {
      FaseService.misPermisos(phaseID).then(perms => {
        commit("setMyPhasePerms", perms);
      });
    }
  },
  modules: {}
});
