## Con Docker

### Requerimientos
- Docker

### Instalación
Contruir las imágenes:
```
docker-compose build
```

### Uso
Para hacer correr el proyecto en modo desarrollo:
```
docker-compose up db
docker-compose up dev
docker-compose up frontend
```
O para correr en segundo plano (detached mode):
```
docker-compose up -d db
docker-compose up -d dev
docker-compose up -d frontend
```
> Obs: No ejecutar `docker-compose up` solamente, se deben levantar los contenedores en order y por separado. Al correr en detached mode podría tardar unos minutos en correr el frontend.

Correr los tests: 
```
docker-compose run dev python manage.py test
```
## Sin Docker

### Requerimientos
- Python 3.7
- Node 12.16.0
- PostgreSQL
- @angular/cli 8.0.2
- Pipenv
### Instalación
1. Se debe crear la base de datos `dockerdjango` con el usuario `dockerdjango` como propietario y password `dockerdjango`
2. Instalar las dependencias del backend (en el directorio `code/`):
    ```
    pipenv install -r requirements.txt
    pipenv shell
    python manage.py migrate
    exit
    ```
3. Instalar las dependencias del frontend (en el directorio `frontend/`):
    ```
    npm install
    ```
### Uso
- Para hacer correr el servidor en modo de desarrollo (en el directorio `code/`):
    ```
    pipenv shell
    python manage.py runserver
    ```
- Para hacer correr el frontend en modo de desarrollo (en el directorio `frontend/`):
    ```
    ng serve
    ```