#se ejecuta el bash asi: bash script.sh tag_nombre ambiente_de_trabajo 
echo "Ingresando..."
echo "Clonando repositorio git"
rm -Rf is2
git clone https://gitlab.com/kukiamarilla/is2.git
cd is2
echo "Haciendo checkout de tag"
git checkout $1
if [ $2 = 'produccion' ]
then
    echo "Ingresando a Produccion"
    bash deploy.sh
else
    echo "Ingresando a Desarrollo"
    bash install.sh
fi
