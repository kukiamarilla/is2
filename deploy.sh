echo "1. Conectando con heroku..."
heroku login
cd code 
echo "2. Eliminando entorno virual..."
rm -Rf venv > /dev/null
echo "3. Creando entorno virual..."
virtualenv -p python3 venv
echo "4. Iniciando entorno virual..."
source venv/bin/activate
echo "5. Instalando dependencias..."
pip install -r requirements.txt
echo "6. Eliminando entorno vitual"
rm -Rf venv > /dev/null
cd ..
echo "7. Clonando repositorio de Backend de heroku..."
rm -Rf heroku
mkdir heroku
cd heroku
git init
heroku git:remote -a is2-approver-backend
git pull heroku master
echo "8. Actualizado repositorio..."
rm -Rf *
cp -rf ../code/* .
echo "9. Desplegando Backend..."
git add .
git commit -m "Actualización"
if ! git push heroku master ; then
    echo "Falló el despliegue del backend. Abortando."
    exit
fi

echo -n "Ingrese escenario a cargar (1-4): "
read escenario

heroku run python manage.py migrate -a is2-approver-backend
heroku run python manage.py flush -a is2-approver-backend
heroku run python manage.py loaddata dockerdjango/api/fixtures/escenarios/esc$escenario.json -a is2-approver-backend

cd ..
rm -rf frontend/node_modules > /dev/null
echo "10. Clonando repositorio de Frontend de heroku..."
rm -Rf heroku
mkdir heroku
cd heroku
git init
heroku git:remote -a is2-approver
git pull heroku master
echo "11. Actualizado repositorio..."
rm -Rf *
cp -rf ../frontend/* .
echo "12. Desplegando Frontend..."
git add .
git commit -m "Actualización"
if ! git push heroku master ; then
    echo "Falló el despliegue del frontend. Abortando."
    exit
fi
cd ..
echo "13. Limpiando..."
rm -Rf heroku > /dev/null
echo "El proyecto fue desplegado en producción exitosamente!"


