echo "Ingresar var de entorno..."

# while : ; do
#     read -n 1 -s key <&1 > /dev/null
#     if [[ $key = q ]] ; then
#         exit
#     else
#         break
#     fi
# done

echo -n "Firebase Api Key: "
read fApiKey
echo -n "Firebase Auth Domain: "
read fAuthDomain
echo -n "Firebase database url: "
read fDatabaseUrl
echo -n "Firebase Project ID: "
read fProjectId
echo -n "Firebase Storage Bucket: "
read fStorageBucket
echo -n "Firebase Messaging Sender ID: "
read fMessagingSenderId
echo -n "Firebase App ID: "
read fAppId
echo -n "Database Name: "
read dbName
echo -n "Database User: "
read dbUser
echo -n "Database Password: "
read -s dbPassword
echo " "
echo -n "Mail Sender Email: "
read msEmail
echo -n "Mail Sender Password: "
read -s msPassword
echo " "
echo -n "Ingrese escenario a cargar (1-4): "
read escenario

#if linux
if [ "$OSTYPE" = "linux-gnu" ]
then
    sudo su postgres <<EOF
    psql -c "DROP DATABASE $dbName"
    psql -c "DROP ROLE $dbUser"
    psql -c "CREATE USER $dbUser WITH PASSWORD '$dbPassword'"
    psql -c "ALTER USER $dbUser CREATEDB"
    psql -c "CREATE DATABASE $dbName OWNER $dbUser"
    echo "Postgres user '$dbUser' y database '$dbName' creado"
EOF
elif [ "$OSTYPE" = "darwin19" ]
then
    psql -U postgres -c "DROP DATABASE $dbName"
    psql -U postgres -c "DROP ROLE $dbUser"
    psql -U postgres -c "CREATE USER $dbUser WITH PASSWORD '$dbPassword'"
    psql -U postgres -c "ALTER USER $dbUser CREATEDB"
    psql -U postgres -c "CREATE DATABASE $dbName OWNER $dbUser"
    echo "Postgres user '$dbUser' y database '$dbName' creado"
fi

cd code
cd dockerdjango
#.env en code
{
echo 'FIREBASE_API_KEY='$fApiKey
echo 'FIREBASE_AUTH_DOMAIN='$fAuthDomain
echo 'FIREBASE_DATABASE_URL='$fDatabaseUrl
echo 'FIREBASE_STORAGE_BUCKET='$fStorageBucket
echo 'DATABASE_NAME='$dbName
echo 'DATABASE_USER='$dbUser
echo 'DATABASE_PASSWORD='$dbPassword
echo 'TESTING_USER_EMAIL=test@is2.com'
echo 'TESTING_USER_PASSWORD=testing'
echo 'MAIL_SENDER_EMAIL='$msEmail
echo 'MAIL_SENDER_PASSWORD='$msPassword 
} > .env
cd ../
echo "INSTALANDO PROYECTO DJANGO"
echo "Creando entorno virtual..."
rm -Rf venv > /dev/null
virtualenv -p python3 venv
echo ""
echo "Ingresando al entorno virtual..."
source venv/bin/activate
echo ""
echo "Intalando dependecias..."
pip install -r requirements.txt
echo ""
echo "Migrando DB..."
python manage.py migrate
echo ""
echo "Poblando DB..."
python manage.py flush
python manage.py loaddata dockerdjango/api/fixtures/escenarios/esc$escenario.json
echo ""
echo "PROYECTO DJANGO INSTALADO"
deactivate
echo ""


echo "INSTALANDO PROYECTO VUE"
cd ../frontend
echo "Instalando dependencias npm..."
rm -Rf node_modules > /dev/null
npm install

#.env.local  en frontend
{
echo 'VUE_APP_API_KEY='$fApiKey
echo 'VUE_APP_AUTH_DOMAIN='$fAuthDomain
echo 'VUE_APP_DATABASE_URL='$fDatabaseUrl
echo 'VUE_APP_PROJECT_ID='$fProjectId
echo 'VUE_APP_STORAGE_BUCKET='$fStorageBucket
echo 'VUE_APP_MESSAGING_SENDER_ID='$fMessagingSenderId
echo 'VUE_APP_APP_ID='$fAppId
echo 'VUE_APP_API_URL=http://localhost:8000/api/'
echo 'MAIL_SENDER_EMAIL=' $email
echo 'MAIL_SENDER_PASSWORD=' $password
} > .env.local

echo ""
echo "PROYECTO VUE INSTALADO"
