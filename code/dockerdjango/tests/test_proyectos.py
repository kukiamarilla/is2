import datetime
from django.test.testcases import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from dockerdjango.api.models import Usuario, Proyecto, Rol, RolProyecto, Fase, Comite
from .payload_client import PayloadClient


class ProyectosTestCase(TestCase):
    client_class = PayloadClient
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
        "dockerdjango/api/fixtures/testing/proyectos.json",
        "dockerdjango/api/fixtures/testing/rol_proyectos.json",
        "dockerdjango/api/fixtures/testing/fases.json",
    ]

    def test_list_proyectos(self):
        print("\nProbando listado de proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/proyectos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), Proyecto.objects.count())

    def test_retrieve_proyectos(self):
        print("\nProbando obtencion de proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/proyectos/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 1)

    def test_create_proyectos(self):
        print("\nProbando creación de proyectos")
        proyecto = {
            "nombre": "Proyecto 3",
            "duracion": 4,
            "fecha_inicio": (datetime.date.today() + datetime.timedelta(days=1)).isoformat(),
            "gerente_id": 1
        }
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/proyectos/", proyecto)
        body = response.json()
        self.assertEquals(response.status_code, 200)
        proyectos = Proyecto.objects.filter(**proyecto)
        self.assertEquals(len(proyectos), 1)
        u = Usuario.objects.get(pk=1)
        p = Proyecto.objects.get(pk=body["id"])
        rp = RolProyecto.objects.filter(usuario=u, proyecto=p)
        self.assertEquals(rp.count(), 1)
        self.assertEquals(rp[0].rol.group.name, "Gerente")

    def test_edit_proyectos(self):
        print("\nProbando edición de proyectos")
        proyecto = {
            "nombre": "Proyecto 3",
            "duracion": 4,
            "fecha_inicio": (datetime.date.today() + datetime.timedelta(days=1)).isoformat(),
        }
        self.client.login(username="testing", password="testing")
        response = self.client.put(
            "/api/proyectos/1/", proyecto, content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        proyectos = Proyecto.objects.filter(**proyecto)
        self.assertEquals(len(proyectos), 1)
        self.assertEquals(proyectos[0].id, 1)

    def test_delete_proyectos(self):
        print("\nProbando eliminar de proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.delete("/api/proyectos/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        proyectos = Proyecto.objects.filter(pk=1)
        self.assertEquals(len(proyectos), 0)

    def test_list_fases_from_proyectos(self):
        print("\nProbando listar fases de proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/proyectos/1/fases/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 2)

    def test_list_miembros_from_proyectos(self):
        print("\nProbando listar miembros de proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/proyectos/1/miembros/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 3)

    def test_add_miembros_to_proyectos(self):
        print("\nProbando asignación de miembros a proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/proyectos/1/miembros/", {"usuario_id": 2, "rol_id": 2})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        rp = RolProyecto.objects.filter(proyecto_id=1)
        self.assertEquals(len(rp), 4)
        rp = RolProyecto.objects.filter(proyecto_id=1, usuario_id=1)
        self.assertEquals(len(rp), 1)
        self.assertEquals(rp[0].rol_id, 2)

    def test_delete_miembros_to_proyectos(self):
        print("\nProbando eliminación de miembros de proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.delete_with_data(
            "/api/proyectos/1/miembros/", {"usuario_id": 4})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        rp = RolProyecto.objects.filter(proyecto_id=1)
        self.assertEquals(len(rp), 2)

    def test_start_proyectos(self):
        print("\nProbando iniciar proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/proyectos/1/iniciar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        p = Proyecto.objects.get(pk=1)
        self.assertEquals(p.estado, "I")

    def test_add_comite_to_proyectos(self):
        print("\nProbando asignación de comité al proyectos")
        u = Usuario.objects.get(pk=5)
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/proyectos/1/comite/", [{"id": u.id}], content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 201)
        c = Comite.objects.filter(proyecto_id=1)
        self.assertEquals(len(c), 1)
        c = Comite.objects.filter(proyecto_id=1, miembros=u)
        self.assertEquals(len(c), 1)

    def test_list_comite_from_proyectos(self):
        print("\nProbando listado de miembros de comité del proyectos")
        c = Comite.objects.create(proyecto_id=1, miembros_id=5)
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/proyectos/1/comite/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 1)

    def test_my_permissions_from_proyectos(self):
        print("\nProbando listado de permisos del usuario en el proyectos")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/proyectos/1/mis_permisos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 14)

    def test_cancelar_proyecto(self):
        print("\nProbando cancelar proyecto")
        self.client.login(username="testing", password="testing")
        pry = Proyecto.objects.get(pk=1)
        pry.estado = "I"
        pry.save()
        response = self.client.post("/api/proyectos/1/cancelar/", content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Proyecto.objects.get(pk=1).estado, "X")

    def test_terminar_proyecto(self):
        print ("\nProbando terminar proyecto")
        self.client.login(username="testing", password="testing")
        pry = Proyecto.objects.get(pk=3)
        response = self.client.post("/api/proyectos/3/terminar/", content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Proyecto.objects.get(pk=3).estado, "T")



