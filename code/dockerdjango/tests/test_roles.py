from django.test import TestCase
from django.test import Client
from dockerdjango.api.models import Rol
from django.contrib.auth.models import Group
from dockerdjango.tests.payload_client import PayloadClient


class RolesTestCase(TestCase):
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
    ]
    client_class = PayloadClient

    def test_list_permissions(self):
        print("\nProbando listado de permisos.")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/permisos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)

    def test_list_roles(self):
        print("\nProbando listado de roles.")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/roles/")
        body = response.json()
        self.assertEquals(response.status_code, 200)

    def test_retrieve_rol(self):
        print("\nProbando obtención de rol.")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/roles/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)

    def test_create_roles(self):
        print("\nProbando creación de rol.")
        rol = {
            "name": "Rol 6",
            "tipo": "S"
        }
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/roles/", rol)
        body = response.json()
        self.assertEquals(response.status_code, 200)
        group_from_db = Group.objects.filter(name=rol['name'])
        self.assertEquals(len(group_from_db), 1)
        rol_from_db = group_from_db[0].rol
        self.assertEquals(rol_from_db.tipo, rol["tipo"])

    def test_delete_roles(self):
        print("\nProbando eliminación de rol.")
        rol = Rol.objects.get(pk=1)
        group = rol.group
        self.client.login(username="testing", password="testing")
        response = self.client.delete("/api/roles/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        rol_from_db = Rol.objects.filter(pk=rol.pk)
        self.assertEquals(len(rol_from_db), 0)
        group_from_db = Group.objects.filter(pk=group.pk)
        self.assertEquals(len(group_from_db), 0)

    def test_list_permissions_from_rol(self):
        print("\nProbando eliminación de rol.")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/roles/1/permisos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 12)

    def test_add_permissions_to_rol(self):
        print("\nProbando agregación de permiso a rol.")
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/roles/1/permisos/", {"permiso_id": 58})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body["group"]["permissions"]), 13)

    def test_delete_permissions_from_rol(self):
        print("\nProbando eliminación de permiso de rol.")
        self.client.login(username="testing", password="testing")
        response = self.client.delete_with_data(
            "/api/roles/1/permisos/", {"permiso_id": 41})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body["group"]["permissions"]), 11)
