from django.test.testcases import TestCase
from django.test.client import Client
from django.contrib.auth.models import Group, User
from dockerdjango.api.models import Usuario
from .payload_client import PayloadClient


class UsuariosTestCase(TestCase):
    client_class = PayloadClient
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
    ]

    def test_list_usuario(self):
        print("\nProbando listado de usuarios")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/usuarios/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), Usuario.objects.count())

    def test_retrieve_usuario(self):
        print("\nProbando obtención de usuario")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/usuarios/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 1)

    def test_activate_user(self):
        print("\nProbando activación de usuario")
        user = Usuario.objects.get(pk=1)
        user.estado = "I"
        user.save()
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/usuarios/1/activar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        user = Usuario.objects.get(pk=1)
        self.assertEquals(user.estado, "A")

    def test_deactivate_user(self):
        print("\nProbando desactivación de usuario")
        user = Usuario.objects.get(pk=1)
        user.estado = "A"
        user.save()
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/usuarios/1/desactivar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        user = Usuario.objects.get(pk=1)
        self.assertEquals(user.estado, "I")

    def test_retrieve_permissions_from_users(self):
        print("\nProbando obtención permisos de usuario logueado.")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/usuarios/2/permisos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 12)

    def test_retrieve_roles_from_users(self):
        print("\nProbando obtención roles de usuario.")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/usuarios/2/roles/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 1)

    def test_add_roles_to_users(self):
        print("\nProbando agregación roles de usuario.")
        user = Usuario.objects.get(pk=2).user
        group = Group.objects.get(name="Administrador")
        user.groups.remove(group)
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/usuarios/2/roles/", {"rol_id": group.rol.id})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        user = Usuario.objects.get(pk=2).user
        self.assertEquals(user.groups.count(), 1)

    def test_delete_roles_from_users(self):
        print("\nProbando eliminacion roles de usuario.")
        group = Group.objects.get(name="Administrador")
        self.client.login(username="testing", password="testing")
        response = self.client.delete_with_data(
            "/api/usuarios/2/roles/", {"rol_id": group.rol.id})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        user = Usuario.objects.get(pk=2).user
        self.assertEquals(user.groups.count(), 0)
