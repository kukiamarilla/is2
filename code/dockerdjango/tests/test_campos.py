from django.test import TestCase, Client
from .payload_client import PayloadClient
from dockerdjango.api.models import Fase, RolFase, TipoItem, Campo


class CamposTestCase(TestCase):
    class_name = PayloadClient
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
        "dockerdjango/api/fixtures/testing/proyectos.json",
        "dockerdjango/api/fixtures/testing/rol_proyectos.json",
        "dockerdjango/api/fixtures/testing/fases.json",
        "dockerdjango/api/fixtures/testing/tipo_items.json",
        "dockerdjango/api/fixtures/testing/campos.json",
        "dockerdjango/api/fixtures/testing/rol_fases.json",
    ]

    def test_list_campos(self):
        print("\nProbando listado de campos personalizados")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/campos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 4)

    def test_retrieve_campos(self):
        print("\nProbando obtención de campos personalizados")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/campos/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 1)

    def test_create_campos(self):
        print("\nProbando creación de campos personalizados")
        self.client.login(username="testing", password="testing")
        campo = {
            "nombre": "Campo 3",
            "tipo_dato": "String",
            "tipo_item_id": 1
        }
        response = self.client.post("/api/campos/", campo)
        body = response.json()
        self.assertEquals(response.status_code, 200)
        campo = Campo.objects.filter(**campo)
        self.assertEquals(len(campo), 1)

    def test_edit_campos(self):
        print("\nProbando edición de campos personalizados")
        self.client.login(username="testing", password="testing")
        campo = {
            "nombre": "Campo 3",
            "tipo_dato": "String",
            "tipo_item_id": 1
        }
        response = self.client.put(
            "/api/campos/1/", campo, content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        campo = Campo.objects.get(**campo)
        self.assertEquals(campo.id, 1)

    def test_delete_campos(self):
        print("\nProbando edición de campos personalizados")
        self.client.login(username="testing", password="testing")
        response = self.client.delete("/api/campos/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        campo = Campo.objects.filter(pk=1)
        self.assertEquals(len(campo), 0)
