from django.test import TestCase, Client
from .payload_client import PayloadClient
from dockerdjango.api.models import Fase, RolFase, TipoItem


class TipoItemsTestCase(TestCase):
    client_class = PayloadClient
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
        "dockerdjango/api/fixtures/testing/proyectos.json",
        "dockerdjango/api/fixtures/testing/rol_proyectos.json",
        "dockerdjango/api/fixtures/testing/fases.json",
        "dockerdjango/api/fixtures/testing/tipo_items.json",
        "dockerdjango/api/fixtures/testing/campos.json",
        "dockerdjango/api/fixtures/testing/rol_fases.json",
    ]

    def test_list_tipo_items(self):
        print("\nProbando listado de tipo de items")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/tipo-item/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 6)

    def test_retrieve_tipo_items(self):
        print("\nProbando obtención de tipo de items")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/tipo-item/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 1)

    def test_create_tipo_items(self):
        print("\nProbando creación de tipo de items")
        tipo_item = {"nombre": "Tipo 5", "fase_id": 1}
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/tipo-item/", tipo_item)
        body = response.json()
        self.assertEquals(response.status_code, 200)
        tipo_item = TipoItem.objects.filter(**tipo_item)
        self.assertEquals(len(tipo_item), 1)

    def test_edit_tipo_items(self):
        print("\nProbando edición de tipo de items")
        tipo_item = {
            "nombre": "Tipo 5",
        }
        self.client.login(username="testing", password="testing")
        response = self.client.put(
            "/api/tipo-item/1/", tipo_item, content_type="application/json"
        )
        body = response.json()
        self.assertEquals(response.status_code, 200)
        tipo_item = TipoItem.objects.get(pk=1)
        self.assertEquals(tipo_item.nombre, "Tipo 5")

    def test_delete_tipo_items(self):
        print("\nProbando eliminación de tipo de items")
        self.client.login(username="testing", password="testing")
        response = self.client.delete("/api/tipo-item/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        tipo_item = TipoItem.objects.filter(pk=1)
        self.assertEquals(len(tipo_item), 0)

    def test_campos_from_tipo_items(self):
        print("\nProbando listado de campos personalizados de tipo de items")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/tipo-item/1/campos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 2)
