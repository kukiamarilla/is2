from django.test import TestCase, Client
from .payload_client import PayloadClient
from dockerdjango.api.models import Item


class ItemTestCase(TestCase):
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
        "dockerdjango/api/fixtures/testing/proyectos.json",
        "dockerdjango/api/fixtures/testing/rol_proyectos.json",
        "dockerdjango/api/fixtures/testing/fases.json",
        "dockerdjango/api/fixtures/testing/tipo_items.json",
        "dockerdjango/api/fixtures/testing/campos.json",
        "dockerdjango/api/fixtures/testing/rol_fases.json",
        "dockerdjango/api/fixtures/testing/items.json",
        "dockerdjango/api/fixtures/testing/versiones.json",
        "dockerdjango/api/fixtures/testing/lineas_base.json",
        "dockerdjango/api/fixtures/testing/relaciones.json",
    ]

    def test_list_items(self):
        print("\nProbando listado de items")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/items/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), Item.objects.count())

    def test_retrieve_items(self):
        print("\nProbando obtencion de items")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/items/2/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 2)

    def test_approve_items(self):
        print("\nProbando aprobacion de items")
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/items/5/aprobar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        item = Item.objects.get(pk=5)
        self.assertEquals(item.estado, "A")

    def test_disapprove_items(self):
        print("\nProbando desaprobacion de items")
        item = Item.objects.get(pk=3)
        item.estado = "A"
        item.save()
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/items/3/desaprobar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        item = Item.objects.get(pk=3)
        self.assertEquals(item.estado, "C")

    def test_deactivate_items(self):
        print("\nProbando desactivación de items")
        item = Item.objects.get(pk=4)
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/items/4/desactivar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        item = Item.objects.get(pk=4)
        self.assertEquals(item.estado, "D")
