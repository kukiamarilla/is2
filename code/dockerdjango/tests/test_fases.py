from django.test import TestCase, Client
from .payload_client import PayloadClient
from dockerdjango.api.models import Fase, RolFase, TipoItem, Item


class FasesTestCase(TestCase):
    client_class = PayloadClient
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
        "dockerdjango/api/fixtures/testing/proyectos.json",
        "dockerdjango/api/fixtures/testing/rol_proyectos.json",
        "dockerdjango/api/fixtures/testing/fases.json",
        "dockerdjango/api/fixtures/testing/tipo_items.json",
        "dockerdjango/api/fixtures/testing/campos.json",
        "dockerdjango/api/fixtures/testing/rol_fases.json",
        "dockerdjango/api/fixtures/testing/items.json"
    ]

    def test_list_fases(self):
        print("\nProbando listado de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/fases/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 4)

    def test_retrieve_fases(self):
        print("\nProbando obtención de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/fases/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 1)

    def test_create_fases(self):
        print("\nProbando creación de fases")
        fase = {
            "nombre": "Fase 3",
            "proyecto_id": 1,
            "usuario_id": 1
        }
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/fases/", fase)
        body = response.json()
        self.assertEquals(response.status_code, 200)
        fase = Fase.objects.filter(id=body["id"])
        self.assertEquals(len(fase), 1)
        self.assertEquals(fase[0].orden, 3)

    def test_edit_fases(self):
        print("\nProbando modificación de fases")
        fase = {
            "nombre": "Fase 3",
            "proyecto_id": 1,
        }
        self.client.login(username="testing", password="testing")
        response = self.client.put(
            "/api/fases/1/", fase, content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        fase = Fase.objects.filter(id=body["id"])
        self.assertEquals(len(fase), 1)
        self.assertEquals(fase[0].nombre, "Fase 3")

    def test_delete_fases(self):
        print("\nProbando eliminación de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.delete(
            "/api/fases/1/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        fase = Fase.objects.filter(id=1)
        self.assertEquals(len(fase), 0)

    def test_my_permissions_fases(self):
        print("\nProbando listado de permisos de usuario logueado en fases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/fases/1/mis_permisos/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 4)

    def test_list_miembros_from_fases(self):
        print("\nProbando listado de miembros de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/fases/1/miembros/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 2)

    def test_add_miembros_from_fases(self):
        print("\nProbando agregar de miembros a fases")
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/fases/1/miembros/", {"usuario_id": 3, "rol_id": 5})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        miembros = RolFase.objects.filter(usuario_id=3)
        self.assertEquals(len(miembros), 1)

    def test_delete_miembros_from_fases(self):
        print("\nProbando eliminación de miembros de fases")
        self.client.login(username="testing", password="testing")
        RolFase.objects.create(usuario_id=4, rol_id=5, fase_id=1)
        response = self.client.delete_with_data(
            "/api/fases/1/miembros/", {"usuario_id": 4})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        miembros = RolFase.objects.filter(usuario_id=4)
        self.assertEquals(len(miembros), 0)

    def test_import_tipo_item_to_fases(self):
        print("\nProbando importación de plantilla de items de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.post(
            "/api/fases/1/importar/", {"tipo_item_id": 3})
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 7)
        tipo_item = TipoItem.objects.get(pk=body["id"])
        self.assertEquals(tipo_item.fase.id, 1)

    def test_listar_tipo_item_from_fases(self):
        print("\nProbando listar tipos de items de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/fases/1/tipo_items/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), 2)

    def test_list_linea_base_from_fase(self):
        print("\nProbando listar linea base de fases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/fases/1/lineas_base/")
        body = response.json()
        self.assertEquals(response.status_code, 200)

    def test_cerrar_fases(self):
        print("\nProbando cerrar fases")
        item= Item.objects.get(pk=3)
        item.estado = "L"
        item.save()
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/fases/1/cerrar/")
        body = response.json()
        self.assertEquals(response.status_code, 200)