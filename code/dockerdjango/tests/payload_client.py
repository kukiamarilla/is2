from django.test import Client


class PayloadClient(Client):
    def delete_with_data(self, *args, **kwargs):
        """ Construct a DELETE request that includes data."""
        kwargs.update({'REQUEST_METHOD': 'DELETE',
                       "content_type": "application/json"})
        return super(PayloadClient, self).put(*args, **kwargs)
