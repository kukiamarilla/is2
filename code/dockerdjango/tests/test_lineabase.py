from django.test import TestCase, Client
from .payload_client import PayloadClient
from dockerdjango.api.models import LineaBase
from dockerdjango.api.models import Solicitud

class LineaBaseTestCase (TestCase):
    fixtures = [
        "dockerdjango/api/fixtures/testing/auth.json",
        "dockerdjango/api/fixtures/testing/group.json",
        "dockerdjango/api/fixtures/testing/permission.json",
        "dockerdjango/api/fixtures/testing/rol.json",
        "dockerdjango/api/fixtures/testing/usuarios.json",
        "dockerdjango/api/fixtures/testing/proyectos.json",
        "dockerdjango/api/fixtures/testing/rol_proyectos.json",
        "dockerdjango/api/fixtures/testing/fases.json",
        "dockerdjango/api/fixtures/testing/tipo_items.json",
        "dockerdjango/api/fixtures/testing/campos.json",
        "dockerdjango/api/fixtures/testing/rol_fases.json",
        "dockerdjango/api/fixtures/testing/items.json",
        "dockerdjango/api/fixtures/testing/versiones.json",
        "dockerdjango/api/fixtures/testing/lineas_base.json",
        "dockerdjango/api/fixtures/testing/relaciones.json",
    ]
    def test_list_linea_base(self):
        print("\nProbando listado de lineas bases")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/lineas-base/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), LineaBase.objects.count())

    def test_retrieve_linea_base(self):
        print("\nProbando obtencion de linea base")
        self.client.login(username="testing", password="testing")
        response = self.client.get("/api/lineas-base/2/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(body["id"], 2)

    def test_create_linea_base(self):
        print("\nProbando creado de lineas bases")
        linea_base = {
            "fase_id": 1,
            "items": [
                {
                    "id": 3
                }
            ]
        }
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/lineas-base/", linea_base, content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        linea_base = LineaBase.objects.filter(id=body["id"])
        self.assertEquals(len(linea_base), 1)
        n = len(LineaBase.objects.filter(fase__proyecto=linea_base[0].fase.proyecto))
        self.assertEquals(linea_base[0].nombre, "LB" + str(n))
        self.assertEquals(linea_base[0].items.all()[0].id, 3)

    """SOLICITUD DE RUPTURA"""

    def test_create_solicitud_ruptura(self):
        print("\nProbando creacion de solicitud")
        solicitud = {
            "usuario_id": 1,
            "operaciones" : 
            [
                {
                    "operacion": "romper",
                    "payload": {
                        "linea_base": 2
                    }
                },
                {
                    "operacion": "aprobar",
                    "payload": {
                        "item": 2
                    }
                },
                {
                    "operacion": "crear",
                    "payload": {
                        "fase": 1,
                        "items": [2]
                    }
                }
            ],
            "motivo":"Solo necesitamos los items relacionados en la linea base"
        }
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/solicitud/", solicitud, content_type="application/json")
        body = response.json()
        self.assertEquals(response.status_code, 200)
    
    def test_list_solicitud_ruptura(self):
        print("\nProbando listado de solicitud")
        self.client.login(username="testing", password="testing")
        Solicitud.objects.create(operacion="crear", usuario_id=1, impacto=4, estado="R", motivo="nc", proyecto_id=1)
        response = self.client.get("/api/solicitud/")
        body = response.json()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(body), Solicitud.objects.count())

    def test_primera_operacion_romper(self):
        print("\nProbando que la primera operacion sea romper")

        solicitud = {
            "usuario_id": 1,
            "operaciones" : 
            [
                {
                    "operacion": "crear",
                    "payload": {
                        "fase": 1,
                        "items": [3]
                    }
                }
            ],
            "motivo":"aberrr"
        }
        self.client.login(username="testing", password="testing")
        response = self.client.post("/api/solicitud/", solicitud, content_type="application/json")
        self.assertEquals(response.status_code, 406)

    