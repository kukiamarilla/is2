from rest_framework import serializers
from dockerdjango.api.models import TipoItem
from .campo import CampoSerializer
from .fase import FaseSerializer

class TipoItemSerializer(serializers.ModelSerializer):
    """
    Serializer de TipoItem
    """
    campos = CampoSerializer(many=True, read_only=True)
    fase = FaseSerializer(many=False, read_only=True)

    class Meta:
        model = TipoItem
        fields = ("id", "nombre", "campos", "fase")