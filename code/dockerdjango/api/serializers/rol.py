from rest_framework import serializers
from dockerdjango.api.models import Rol
from dockerdjango.api.serializers.group import GroupSerializer

class RolSerializer(serializers.ModelSerializer):
    """
    Serializer de Roles
    """
    group = GroupSerializer(many=False, read_only=True)
    
    class Meta:
        model = Rol
        fields = ("id", "tipo", "group")