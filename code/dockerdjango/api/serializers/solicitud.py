from rest_framework import serializers
from dockerdjango.api.models import Solicitud
from dockerdjango.api.serializers import UsuarioSerializer


class SolicitudSerializer(serializers.ModelSerializer):
    """
    Serializer para solicitud
    """

    usuario = UsuarioSerializer(many=False, read_only=True)

    class Meta:
        model = Solicitud
        fields = ("id", "usuario", "operacion", "estado", "motivo", "impacto", "proyecto_id")
