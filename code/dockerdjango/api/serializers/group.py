from rest_framework import serializers
from django.contrib.auth.models import Group
from dockerdjango.api.serializers.permiso import PermisoSerializer

class GroupSerializer(serializers.ModelSerializer):
    """
    Serializer de Groups
    """
    permissions = PermisoSerializer(many=True, read_only=True)
    
    class Meta:
        model = Group
        fields = ("id", "name", "permissions")