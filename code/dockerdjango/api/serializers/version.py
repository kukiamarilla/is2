from rest_framework import serializers

from dockerdjango.api.models import Version

from . import ValorPersonalizadoSerializer


class VersionSerializer(serializers.ModelSerializer):
    """
    Serializer de Version
    Atributos
        valores_personalizados : Valores personalizados de Item por Version
    """

    valores_personalizados = ValorPersonalizadoSerializer(
        many=True, read_only=True)

    class Meta:
        model = Version
        fields = ("id", "numero", "nombre", "peso", "archivo",
                  "item_id", "actual", "valores_personalizados", "padres")
