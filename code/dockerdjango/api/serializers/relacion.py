from rest_framework import serializers
from dockerdjango.api.models import Relacion
from . import VersionSerializer


class RelacionSerializer(serializers.ModelSerializer):
    """
    Serializer de Relacion
    """

    hijo = VersionSerializer(many=False, read_only=True)

    class Meta:
        model = Relacion
        fields = ("id", "padre_id", "hijo_id", "hijo")
