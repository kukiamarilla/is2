from rest_framework import serializers

from dockerdjango.api.models import ValorPersonalizado

from . import CampoSerializer

class ValorPersonalizadoSerializer(serializers.ModelSerializer):
    """
    Serializer de ValorItem
    """

    campo = CampoSerializer(many=False, read_only=True)

    class Meta:
        model = ValorPersonalizado
        fields = ("id", "version_id", "valor", "campo")