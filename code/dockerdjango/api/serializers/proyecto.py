from rest_framework import serializers
from dockerdjango.api.models import Proyecto
from . import UsuarioSerializer
from . import FaseSerializer

class ProyectoSerializer(serializers.ModelSerializer):
    """
    Serializer de Proyecto
    """
    gerente = UsuarioSerializer(many = False, read_only = True)
    miembros = UsuarioSerializer(many = True, read_only = True)
    miembros_comite = UsuarioSerializer(many = True, read_only = True)
    fases = serializers.SerializerMethodField()

    class Meta:
        model = Proyecto
        fields = ("id", "nombre", "estado", "duracion", "gerente", "miembros", "fases", "fecha_inicio", "miembros_comite")

    def get_fases(self, instance):
        fases = instance.fases.all().order_by('orden')
        return FaseSerializer(fases, many=True).data