from rest_framework import serializers
from dockerdjango.api.models import Comite
from . import UsuarioSerializer

class ComiteSerializer(serializers.ModelSerializer):
    """
    Serializer de Comite
    """

    miembros = UsuarioSerializer(many=True, read_only=True)
    class Meta:
        model = Comite
        fields = ("id", "proyecto_id", "miembros")