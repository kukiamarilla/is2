from rest_framework import serializers
from dockerdjango.api.models import Campo

class CampoSerializer(serializers.ModelSerializer):
    """
    Serializer de TipoItem
    """

    class Meta:
        model = Campo
        fields = ("id", "nombre", "tipo_item_id", "tipo_dato")
