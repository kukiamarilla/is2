from rest_framework import serializers
from dockerdjango.api.models import Rol
from dockerdjango.api.serializers.usuario import UsuarioSerializer
from dockerdjango.api.serializers.rol import RolSerializer
from dockerdjango.api.serializers.fase import FaseSerializer

class RolFaseSerializer(serializers.ModelSerializer):
    """
    Serializer de Roles
    """
    rol = RolSerializer(many=False, read_only=True)
    usuario = UsuarioSerializer(many=False, read_only=True)
    fase = FaseSerializer(many=False, read_only=True)
    
    class Meta:
        model = Rol
        fields = ("id", "rol", "fase","usuario")