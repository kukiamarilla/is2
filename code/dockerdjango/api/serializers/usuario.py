from rest_framework import serializers
from dockerdjango.api.models.usuario import Usuario
from dockerdjango.api.serializers.user import UserSerializer


class UsuarioSerializer(serializers.ModelSerializer):
    """
    Serializer de Usuario
    """
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Usuario
        fields = ("id", "user_id", "estado", "user", "firebase_uid")
