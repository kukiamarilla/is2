from rest_framework import serializers
from dockerdjango.api.models import Fase


class FaseSerializer(serializers.ModelSerializer):
    """
    Serializer de Proyecto
    """


    class Meta:
        model = Fase
        fields = ("id", "nombre", "estado", "proyecto_id", "orden")