from rest_framework import serializers

from dockerdjango.api.models import LineaBase

from . import ItemSerializer


class LineaBaseSerializer(serializers.ModelSerializer):
    """
    Serializer de LineaBase
    """

    items = ItemSerializer(many=True, read_only=True)

    class Meta:
        model = LineaBase
        fields = ("id", "nombre", "estado", "items", "fase_id")
