from rest_framework import serializers
from dockerdjango.api.models import Rol
from dockerdjango.api.serializers.usuario import UsuarioSerializer
from dockerdjango.api.serializers.rol import RolSerializer
from dockerdjango.api.serializers.proyecto import ProyectoSerializer

class RolProyectoSerializer(serializers.ModelSerializer):
    """
    Serializer de Roles
    """
    rol = RolSerializer(many=False, read_only=True)
    usuario = UsuarioSerializer(many=False, read_only=True)
    proyecto = ProyectoSerializer(many=False, read_only=True)
    
    class Meta:
        model = Rol
        fields = ("id", "rol", "proyecto","usuario")