from rest_framework import serializers

from dockerdjango.api.models import Item

from . import VersionSerializer, RelacionSerializer


class ItemSerializer(serializers.ModelSerializer):
    """
    Serializer de Item
    Atributos
        versiones : Todas las versiones de Item
        hijos : Todos los hijos de Item
    """

    versiones = VersionSerializer(many=True, read_only=True)
    hijos = RelacionSerializer(many=True, read_only=True)

    class Meta:
        model = Item
        fields = ("id", "fase_id", "fecha_creado",
                  "estado", "versiones", "lineas_base", "hijos")
