# Generated by Django 3.0.2 on 2020-08-19 03:03

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(default='', max_length=50)),
                ('estado', models.CharField(choices=[('A', 'Abierto'), ('C', 'Cerrado')], default='A', max_length=1)),
                ('orden', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Proyecto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(default='', max_length=50)),
                ('estado', models.CharField(choices=[('C', 'Creado'), ('I', 'Iniciado'), ('T', 'Terminado'), ('E', 'Eliminado'), ('X', 'Cancelado')], default='C', max_length=1)),
                ('duracion', models.IntegerField(default=0)),
                ('fecha_inicio', models.DateField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(choices=[('P', 'Proyecto'), ('F', 'Fase'), ('S', 'Sistema')], default='A', max_length=1)),
                ('group', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='auth.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], default='A', max_length=1)),
                ('firebase_uid', models.CharField(default='', max_length=40)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='TipoItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(default='', max_length=50)),
                ('fase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Fase')),
            ],
        ),
        migrations.CreateModel(
            name='RolProyecto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('proyecto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Proyecto')),
                ('rol', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Rol')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Usuario')),
            ],
        ),
        migrations.CreateModel(
            name='RolFase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fase', to='api.Fase')),
                ('rol', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rol', to='api.Rol')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuario', to='api.Usuario')),
            ],
        ),
        migrations.AddField(
            model_name='proyecto',
            name='gerente',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gerente', to='api.Usuario'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='miembros',
            field=models.ManyToManyField(related_name='usuarios', to='api.Usuario'),
        ),
        migrations.AddField(
            model_name='fase',
            name='proyecto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fases', to='api.Proyecto'),
        ),
        migrations.CreateModel(
            name='Campo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(default='', max_length=50)),
                ('tipo_dato', models.CharField(default='', max_length=10)),
                ('tipo_item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='campos', to='api.TipoItem')),
            ],
        ),
    ]
