# Generated by Django 3.0.2 on 2020-08-31 05:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_remove_comite_cantidad'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comite',
            name='miembros',
            field=models.ManyToManyField(to='api.Usuario'),
        ),
    ]
