from django.db import models

from . import Item

class Version(models.Model):
    """
    Modela las versiones de Item
    Atributos 
        numero : Numero de Version
        nombre : Nombre de Item
        peso : Costo de la version
        archivo : Direccion del archivo en firebase
        item : Item relacionado con Version; es decir, un item tiene varias versiones
        actual : Una bandera que nos indica cual version es la mas reciente
    """

    numero = models.IntegerField(default=1)
    nombre = models.CharField(max_length=50)
    peso = models.FloatField()
    archivo = models.CharField(max_length=300)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="versiones")
    actual = models.BooleanField()

