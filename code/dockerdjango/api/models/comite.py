from django.db import models
from . import Usuario
from . import Proyecto

class Comite(models.Model):
    """
    Modelo Comite
    """
    miembros = models.ForeignKey(Usuario, on_delete=models.CASCADE, null=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)