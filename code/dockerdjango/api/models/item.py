from django.db import models
from django.utils import timezone

from . import Fase
from . import TipoItem

class Item(models.Model):
    """
    Modelo para un item dentro del sistema
    Atributos
        fecha_creado : Fecha creada del item
        fase : Fase relacionado con Item; es decir, una fase tiene varios items y cada item esta relacionada a una fase
        ESTADO : Definicion de los diferentes estados
        estado : Estado de Item
        tipo : El tipo relacionado al Item
    """

    fecha_creado = models.DateField()
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE, related_name="items")
    ESTADO = (("A", "Aprobado"), ("C", "Creado"), ("R", "En Revision"), ("D", "Desactivado"), ("L", "En Linea Base"))
    estado = models.CharField(max_length=1, choices=ESTADO, default="C")
    tipo = models.ForeignKey(TipoItem, on_delete=models.CASCADE, null=True)
