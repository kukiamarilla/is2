from django.db import models
from django.contrib.auth.models import User

class Usuario(models.Model):
    """
    Extiende el modelo de Usuarios de Django, relacionado con
    auth.User`.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    ESTADOS = (("A", "Activo"), ("I", "Inactivo"))
    estado = models.CharField(max_length=1, choices=ESTADOS, default="A")
    firebase_uid = models.CharField(max_length=40, default="")
