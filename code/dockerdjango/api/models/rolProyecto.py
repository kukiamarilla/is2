from django.db import models
from .rol import Rol
from .proyecto import Proyecto
from .usuario import Usuario
from django.contrib.auth.models import User


class RolProyecto(models.Model):
    """
    Define el modelo de Roles de Proyecto en el sistema
    """

    rol = models.ForeignKey(Rol, null=False, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(
        Proyecto, null=False, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, null=False, on_delete=models.CASCADE)


def has_any_proyecto_perm(self, proyecto):
    relation = RolProyecto.objects.filter(
        usuario=self.usuario, proyecto=proyecto)
    return len(relation) == 1


def has_proyecto_perm(self, permission, proyecto):
    relation = RolProyecto.objects.filter(
        usuario=self.usuario, proyecto=proyecto)
    if(not self.has_any_proyecto_perm(proyecto)):
        return False
    return len(relation[0].rol.group.permissions.filter(codename=permission)) == 1


User.add_to_class("has_any_proyecto_perm", has_any_proyecto_perm)
User.add_to_class("has_proyecto_perm", has_proyecto_perm)
