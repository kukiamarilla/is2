from django.db import models

from . import Item

class ValorItem(models.Model):
    """
    Modela las versiones de un Item, el cual contiene el valor actual
    """

    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="version")
    

