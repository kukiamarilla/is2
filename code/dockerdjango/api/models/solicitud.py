from django.db import models


class Solicitud(models.Model):
    """
    Define el modelo Solicitud de Cambio
    Atributos
        operacion : Define el/los cambios que un usuario desea realizar. Ejemplo : Romper una a linea base
                                                                                   Aprobar un item
                                                                                   Desaprobar un item
                                                                                   Crear una linea base
        usuario : Usuario que solicita el/los cambios
    """

    operacion = models.TextField(default="")
    usuario = models.ForeignKey("Usuario", on_delete=models.CASCADE)
    proyecto = models.ForeignKey("Proyecto", on_delete=models.CASCADE)
    impacto = models.FloatField(default=0.0)
    ESTADOS = (("R", "Rechazado"), ("P", "Pendiente"), ("A", "Aprobada"))
    estado = models.CharField(max_length=1, choices=ESTADOS, default="P")
    motivo = models.TextField(default="")
