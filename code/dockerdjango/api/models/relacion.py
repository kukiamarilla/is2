from django.db import models
from dockerdjango.api.models import Item, Version


class Relacion(models.Model):
    """
    Define el modelo de Relaciones entre items
    """
    padre = models.ForeignKey(Item, related_name="hijos", on_delete=models.CASCADE)
    hijo = models.ForeignKey(
        Version, related_name="padres", on_delete=models.CASCADE)
