from django.db import models


class Voto(models.Model):
    """
    Modelo de voto, para la aprobacion o rechazo de una solicitud de cambio
    """

    solicitud = models.ForeignKey(
        "Solicitud", on_delete=models.CASCADE, related_name="votos"
    )
    comite = models.ForeignKey("Comite", on_delete=models.CASCADE)
    voto = models.BooleanField(default=False)