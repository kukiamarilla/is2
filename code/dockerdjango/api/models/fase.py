from django.db import models
from .proyecto import Proyecto

class Fase(models.Model):
    """
    Define el modelo de Proyectos en el sistema
    """

    proyecto = models.ForeignKey(Proyecto, null=False, on_delete=models.CASCADE, related_name="fases")
    nombre = models.CharField(max_length=50, default="")
    ESTADOS = (("A", "Abierto"), ("C", "Cerrado"))
    estado = models.CharField(max_length=1, choices=ESTADOS, default="A")
    orden = models.IntegerField(default=0)
    
