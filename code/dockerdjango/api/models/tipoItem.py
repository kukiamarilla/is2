from django.db import models
from dockerdjango.api.models import Fase
class TipoItem(models.Model):
    """
    Define el modelo de Tipo de Item en el sistema
    """

    nombre = models.CharField(max_length=50, default="")
    fase = models.ForeignKey(Fase, null=False, on_delete=models.CASCADE)
