from django.db import models
from . import Usuario
from django.utils import timezone


class Proyecto(models.Model):
    """
    Define el modelo de Proyectos en el sistema
    """
    nombre = models.CharField(max_length=50, default="")
    ESTADO = (("C", "Creado"), ("I", "Iniciado"), ("T", "Terminado"),
              ("E", "Eliminado"), ("X", "Cancelado"))
    estado = models.CharField(max_length=1, choices=ESTADO, default="C")
    duracion = models.IntegerField(default=0)
    miembros = models.ManyToManyField(Usuario, related_name="usuarios")
    gerente = models.ForeignKey(
        Usuario, on_delete=models.CASCADE, related_name="gerente")
    fecha_inicio = models.DateField(default=timezone.now)
    miembros_comite = models.ManyToManyField(
        Usuario, through="Comite", through_fields=("proyecto", "miembros"))
