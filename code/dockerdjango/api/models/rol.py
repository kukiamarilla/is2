from django.db import models
from django.contrib.auth.models import Group 

class Rol(models.Model):
    """
    Define el modelo de Proyectos en el sistema
    """

    TIPOS = (("P", "Proyecto"), ("F", "Fase"), ("S", "Sistema"))
    tipo = models.CharField(max_length=1, choices=TIPOS, default="A")
    group = models.OneToOneField(Group, on_delete=models.CASCADE)
