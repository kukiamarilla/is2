from django.db import models
from .fase import Fase
from .item import Item


class LineaBase(models.Model):
    """
    Define el modelo de Lineas Base en el sistema
    """

    fase = models.ForeignKey(
        Fase, null=False, on_delete=models.CASCADE, related_name="lineas_base")
    nombre = models.CharField(max_length=50, default="")
    ESTADOS = (("C", "Cerrado"), ("R", "Roto"))
    estado = models.CharField(max_length=1, choices=ESTADOS, default="A")
    items = models.ManyToManyField(Item, related_name="lineas_base")
