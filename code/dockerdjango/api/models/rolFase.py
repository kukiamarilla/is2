from django.db import models
from .rol import Rol
from .fase import Fase
from .usuario import Usuario
from django.contrib.auth.models import User

class RolFase(models.Model):
    """
    Define el modelo de Roles de Fases en el sistema
    """


    fase = models.ForeignKey(Fase, null=False, on_delete=models.CASCADE, related_name="fase")
    rol = models.ForeignKey(Rol, null=False, on_delete=models.CASCADE, related_name="rol")
    usuario = models.ForeignKey(Usuario, null=False, on_delete=models.CASCADE, related_name="usuario")

def has_any_fase_perm(self, fase):
    relation = RolFase.objects.filter(usuario=self.usuario, fase=fase)
    return len(relation) == 1


def has_fase_perm(self, permission, fase):
    relation = RolFase.objects.filter(usuario=self.usuario, fase=fase)
    if(not self.has_any_fase_perm(fase)): 
        return False
    return len(relation[0].rol.group.permissions.filter(codename=permission)) == 1


User.add_to_class("has_any_fase_perm", has_any_fase_perm)
User.add_to_class("has_fase_perm", has_fase_perm)
