from django.db import models
from django.utils import timezone
from . import TipoItem

class Campo(models.Model):
    """
    Define el atributo de cada TipoItem en el sistema
    """

    nombre = models.CharField(max_length=50, default="")
    tipo_item = models.ForeignKey(TipoItem, on_delete=models.CASCADE, related_name="campos")
    tipo_dato = models.CharField(max_length=10, default="")
