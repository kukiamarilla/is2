from django.db import models

from . import Item
from . import Version
from . import Campo

class ValorPersonalizado(models.Model):
    """
    Modela un valor personalizado de Item
    Atributos
        version : Version relacionado con ValorPersonalizado; es decir, una version tiene varios valores personalizados
        campo : El campo relacionado con el valor
        nombre : Nombre del campo
        valor : Valor Personalizado
    """

    version = models.ForeignKey(Version, on_delete=models.CASCADE, related_name="valores_personalizados")
    campo = models.ForeignKey(Campo, on_delete=models.CASCADE, null = True)
    valor = models.CharField(max_length=100, default="")
    

