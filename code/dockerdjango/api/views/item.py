from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action
from datetime import date
from django.shortcuts import get_object_or_404
from django.conf import settings
from dockerdjango.api.models import Item
from dockerdjango.api.models import Fase
from dockerdjango.api.models import Version
from dockerdjango.api.models import ValorPersonalizado
from dockerdjango.api.models import Campo
from dockerdjango.api.models import TipoItem
from dockerdjango.api.models import Rol
from dockerdjango.api.models import Usuario
from dockerdjango.api.models import RolFase
from dockerdjango.api.models import Relacion
from dockerdjango.api.serializers import ItemSerializer
from dockerdjango.api.serializers import VersionSerializer
from dockerdjango.api.helpers.firebase import get_file_from_firebase
import pyrebase
import os
import mimetypes
from google.cloud import storage
import smtplib
from django.http import FileResponse, HttpResponse
from dockerdjango.api.helpers.mail import sendmail
from django.db.models import Q


class ItemViewSet(viewsets.ViewSet):
    """
    Modelo para View de Item
    """

    def list(self, request):
        """
        Lista todos los items
        """
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea un Item
        """
        fase = get_object_or_404(Fase, pk=request.POST["fase_id"])
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        plantilla = get_object_or_404(
            TipoItem, pk=request.POST["tipo_item_id"], fase=fase)

        item = Item.objects.create(
            fecha_creado=date.today(),
            fase=fase,
            tipo=plantilla
        )

        archivo_path_on_cloud = ""

        if len(request.FILES) != 0:
            # Recibimos el archivo
            archivo = request.FILES["file"]

            # Recibimos la config de firebase
            firebase = pyrebase.initialize_app(settings.FIREBASE_CLIENT_CONFIG)
            storage = firebase.storage()

            # Ruta del archivo en firebase (cloud)
            archivo_path_on_cloud = "items/" + \
                str(item.id) + "/files/" + archivo.name

            # Almacenamos el archivo en la nube
            storage.child(archivo_path_on_cloud).put(archivo)

        primera_version = Version.objects.create(
            numero=1,
            nombre=request.POST["nombre"],
            peso=request.POST["peso"],
            archivo=archivo_path_on_cloud,
            item=item,
            actual=True
        )

        # Guardamos los valores de cada campo
        campos = item.tipo.campos.all()
        for campo in campos:
            ValorPersonalizado.objects.create(
                version=primera_version,
                campo=campo,
                valor=request.POST[campo.nombre]
            )

        # Guardamos los padres
        padres = request.POST.getlist("padres", [])
        for padre in padres:
            padre = Item.objects.get(pk=padre)
            if (padre.estado != "L"):
                item.delete()
                response = {"message": "Un padre no está en linea base"}
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            if (padre.fase.orden != item.fase.orden and padre.fase.orden != item.fase.orden - 1):
                item.delete()
                response = {
                    "message": "Un padre no esta en la fase actual o en la anterior"}
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            Relacion.objects.create(padre=padre, hijo=primera_version)
        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    def update(self, request, pk=None):
        """
        Modifica un Item
        """
        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if item.estado == "A":
            response = {"message": "No puede modificar un item aprobado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        if item.estado == "R":
            response = {"message": "No puede modificar un item en revision"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        if item.estado == "D":
            response = {"message": "No puede modificar un item desactivado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        if item.estado == "L":
            response = {"message": "No puede modificar un item en linea base"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        num_versiones = item.versiones.all().count()
        ant_version = item.versiones.all().order_by("numero")[
            num_versiones - 1]
        ant_version.actual = False
        ant_version.save()

        archivo_path_on_cloud = ant_version.archivo

        # Recibimos la config de firebase
        firebase = pyrebase.initialize_app(settings.FIREBASE_CLIENT_CONFIG)
        storage = firebase.storage()

        if len(request.FILES) != 0:
            archivo = request.FILES["file"]
            archivo_path_on_cloud = "items/" + \
                str(item.id) + "/files/" + archivo.name
            # Almacenamos el archivo en la nube
            storage.child(archivo_path_on_cloud).put(archivo)

        nueva_version = Version.objects.create(
            numero=num_versiones + 1,
            nombre=request.POST["nombre"],
            peso=request.POST["peso"],
            archivo=archivo_path_on_cloud,
            item=item,
            actual=True
        )
        # Guardamos los padres
        padres = request.POST.getlist("padres", [])
        for padre in padres:
            padre = Item.objects.get(pk=int(padre))
            if (padre.estado != "L"):
                nueva_version.delete()
                ant_version.actual = True
                ant_version.save()
                response = {"message": "Un padre no está en linea base"}
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            if (padre.fase.orden != item.fase.orden and padre.fase.orden != item.fase.orden - 1):
                nueva_version.delete()
                ant_version.actual = True
                ant_version.save()
                response = {
                    "message": "Un padre no esta en la fase actual o en la anterior"}
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            Relacion.objects.create(padre=padre, hijo=nueva_version)

        # Modificamos los valores de cada campo
        campos = item.tipo.campos.all()
        for campo in campos:
            ValorPersonalizado.objects.create(
                version=nueva_version,
                campo=campo,
                valor=request.POST[campo.nombre]
            )

        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene un item
        """

        item = get_object_or_404(Item, pk=pk)

        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def versiones(self, request, pk=None):
        """
        Lista las versiones de un Item
        """

        item = get_object_or_404(Item, pk=pk)
        serializer = VersionSerializer(item.versiones, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["POST"])
    def solicitar_aprobacion(self, request, pk=None):
        """
        Solicita la aprobacion de un Item via email
        """
        # Obtenemos el Item
        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        version = Version.objects.get(item=item, actual=True)

        if item.estado != "C":
            response = {
                "message": "No puede solicitar aprobación de un item que no está desaprobado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        # Obtenemos el usuario al cual enviaremos el mail
        usuarios = RolFase.objects.filter(
            fase=item.fase, rol__group__name="QA")

        msg = """
            El usuario {} ha solicitado aprobacion del item #{}:
            Nombre: {}
            Fase: {}
            
        """ .format(request.user.id, item.id, version.nombre, item.fase.nombre)

        print(request.get_host())

        for usuario in usuarios:
            sendmail(usuario.usuario.user.email, msg,
                     "Solicitud de aprobacion de item")
        response = {"message": "Solicitud Enviada"}
        return Response(response, status=status.HTTP_202_ACCEPTED)

    @action(detail=True, methods=["POST"])
    def solicitar_desaprobacion(self, request, pk=None):
        """
        Solicita la desaprobacion de un Item via email
        """

        # Obtenemos el Item
        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        version = Version.objects.get(item=item, actual=True)
        if item.estado != "A":
            response = {
                "message": "No puede solicitar desprobación de un item que no está aprobado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        # Obtenemos el usuario al cual enviaremos el mail
        usuarios = RolFase.objects.filter(
            fase=item.fase, rol__group__name="QA")

        msg = """
            El usuario {} ha solicitado desaprobacion del item #{}:
            Nombre: {}
            Fase: {}
            Motivo: {}

        """ .format(request.user.id, item.id, version.nombre, item.fase.nombre, request.data["motivo"])

        print(request.get_host())

        for usuario in usuarios:
            sendmail(usuario.usuario.user.email, msg,
                     "Solicitud de desaprobacion de item")
        response = {"message": "Solicitud Enviada"}
        return Response(response, status=status.HTTP_202_ACCEPTED)

    @action(detail=True, methods=["POST"])
    def aprobar(self, request, pk=None):
        """
        Aprueba un item
        """
        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if item.estado == "A":
            response = {"message": "No se puede aprobar un item aprobado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if item.estado == "D":
            response = {"message": "No se puede aprobar un item desactivado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if item.estado == "L":
            response = {"message": "No se puede aprobar un item en linea base"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        item.estado = "A"
        item.save()

        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["POST"])
    def desaprobar(self, request, pk=None):
        """
        Desaprueba un Item
        """

        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if item.estado == "D":
            response = {
                "message": "No se puede desaprobar un item desactivado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if item.estado == "C":
            response = {
                "message": "No se puede desaprobar un item no aprobado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if item.estado == "L":
            response = {
                "message": "No se puede desaprobar un item en linea base"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if item.estado == "R":
            response = {
                "message": "No se puede desaprobar un item en revision"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        item.estado = "C"
        item.save()

        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["POST"])
    def desactivar(self, request, pk=None):
        """
        Desactiva un Item
        """

        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if item.estado == "D":
            response = {"message": "El item ya esta desactivado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if item.estado == "R":
            response = {
                "message": "No se puede desactivar un item en revision"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if item.estado == "L":
            response = {
                "message": "No se puede desactivar un item en linea base"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        item.estado = "D"
        item.save()

        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def archivo(self, request, pk=None):
        """ Obtiene el archivo del Item """

        item = get_object_or_404(Item, pk=pk)
        version = Version.objects.get(item=item, actual=True)
        basename = os.path.basename(version.archivo)
        mime = mimetypes.guess_type(basename)
        file = get_file_from_firebase(version.archivo)
        response = HttpResponse(file, content_type=mime)
        response['Content-Disposition'] = 'attachment; filename="' + \
            basename + '"'
        return response

    @action(detail=True, methods=["POST"])
    def revertir(self, request, pk=None):
        """ Obtiene el archivo del Item """

        item = get_object_or_404(Item, pk=pk)
        if (item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        version = get_object_or_404(Version, pk=request.data["version_id"])

        if item.estado != "C":
            response = {"message": "No puede revertir un item aprobado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if item.id != version.item.id:
            response = {"message": "Esta versión no pertenece a este item"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if version.actual == True:
            response = {"message": "No puede revertir a la version actual"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        Version.objects.filter(item=item).update(actual=False)
        version.pk = None
        version.save()
        version.actual = True
        version.numero = len(Version.objects.filter(item=item))
        version.save()

        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def posibles_padres(self, request, pk=None):
        """ Obtiene el archivo del Item """

        item = get_object_or_404(Item, pk=pk)

        padres = Item.objects.filter(Q(fase__proyecto=item.fase.proyecto) & (
            Q(fase__orden=item.fase.orden) | Q(fase__orden=item.fase.orden-1)) & Q(estado="L") & ~Q(id=item.id))

        serializer = ItemSerializer(padres, many=True)
        return Response(serializer.data)
