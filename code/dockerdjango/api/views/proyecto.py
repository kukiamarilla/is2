from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action
from dockerdjango.api.models import Proyecto
from dockerdjango.api.models import Fase
from dockerdjango.api.models import Usuario
from dockerdjango.api.models import Rol
from dockerdjango.api.models import RolProyecto
from dockerdjango.api.models import Comite
from dockerdjango.api.models import Voto
from dockerdjango.api.models import Solicitud
from dockerdjango.api.serializers import UsuarioSerializer
from dockerdjango.api.serializers import ProyectoSerializer
from dockerdjango.api.serializers import FaseSerializer
from dockerdjango.api.serializers import RolProyectoSerializer
from dockerdjango.api.serializers import ComiteSerializer
from dockerdjango.api.serializers import SolicitudSerializer
from dockerdjango.api.forms import ProyectoForm
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404
from django.shortcuts import redirect
from django.shortcuts import render


class ProyectoViewSet(viewsets.ViewSet):
    def list(self, request):
        """
        Lista todos los proyectos
        Parametros :
        Retorna : Todos los proyectos dentro del sistema
        """
        """
        proyectos = get_list_or_404(Proyecto)
        if request.method == "GET":
            form = ProyectoForm(request.GET, instance=proyectos)
            if form.is_valid():
                proyectos = form.save()
                return redirect("list")
        else:
            form = ProyectoForm(instance=proyectos)
        return render(request, "", {"form": form})
        """
        proyectos = Proyecto.objects.all()
        if not request.user.has_perm("api.view_proyecto"):
            proyectos = filter(
                lambda p: request.user.has_any_proyecto_perm(p), proyectos
            )
        serializer = ProyectoSerializer(proyectos, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea un proyecto
        Parametros: nombre, gerente
        Retorna: Proyecto con un nombre, y gerente asignado.
        """
        if not request.user.has_perm("api.add_proyecto"):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        gerente = Usuario.objects.get(pk=request.data["gerente_id"])
        proyecto = Proyecto.objects.create(
            nombre=request.data["nombre"],
            estado="C",
            duracion=request.data["duracion"],
            gerente=gerente,
            fecha_inicio=request.data["fecha_inicio"],
        )
        rol = Rol.objects.get(tipo="P", group__name="Gerente")
        RolProyecto.objects.create(proyecto=proyecto, usuario=gerente, rol=rol)
        serializer = ProyectoSerializer(proyecto, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_perm(
            "api.view_proyecto"
        ) and not request.user.has_any_proyecto_perm(proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        serializer = ProyectoSerializer(proyecto, many=False)
        return Response(serializer.data)

    def update(self, request, pk=None):
        """
        Modifica un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if proyecto.estado == "X":
            response = {"message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if proyecto.estado == "T":
            response = {"message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if not request.user.has_perm(
            "api.change_proyecto"
        ) and not request.user.has_proyecto_perm("change_proyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)

        # Si el proyecto esta iniciado no se puede modificar
        if proyecto.estado == "I":
            response = {"message": "No se puede modificar un proyecto iniciado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        proyecto = Proyecto.objects.filter(pk=pk)
        proyecto.update(**request.data)
        proyecto[0].save()
        serializer = ProyectoSerializer(proyecto[0], many=False)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        """
        Elimina un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_perm(
            "api.delete_proyecto"
        ) and not request.user.has_proyecto_perm("delete_proyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        if proyecto.estado == "X":
            response = {"message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if proyecto.estado == "T":
            response = {"message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        # Si el proyecto esta iniciado no se puede eliminar
        if proyecto.estado == "I":
            response = {"message": "No se puede eliminar un proyecto iniciado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if proyecto.estado == "T":
            response = {"message": "No se puede eliminar un proyecto terminado"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        proyecto.delete()
        response = {"message": "Proyecto Eliminado."}
        return Response(response)

    @action(detail=True, methods=["GET"])
    def fases(self, request, pk=None):
        """
        Lista las fases de un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_proyecto_perm("view_fase", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        fases = Fase.objects.filter(proyecto=proyecto)
        serializer = FaseSerializer(fases, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def miembros(self, request, pk=None):
        """
        Lista los miembros de un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_proyecto_perm("view_rolproyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        miembros = RolProyecto.objects.filter(proyecto=proyecto)
        serializer = RolProyectoSerializer(miembros, many=True)
        return Response(serializer.data)

    @miembros.mapping.post
    def agregar_miembro(self, request, pk=None):
        """
        Agrega un miembro al proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_proyecto_perm("add_rolproyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        if proyecto.estado == "X":
            response = {"message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if proyecto.estado == "T":
            response = {"message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=request.data["rol_id"])
        if rol.tipo != "P":
            response = {"message": "Este no es un rol de proyecto"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=request.data["usuario_id"])
        if usuario.user.has_any_proyecto_perm(proyecto):
            response = {"message": "Este usuario ya es miembro del proyecto"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        miembro = RolProyecto.objects.create(
            proyecto=proyecto, usuario=usuario, rol=rol
        )
        miembros = RolProyecto.objects.filter(proyecto=proyecto)
        serializer = RolProyectoSerializer(miembros, many=True)
        return Response(serializer.data)

    @miembros.mapping.delete
    def eliminar_miembro(self, request, pk=None):
        """
        Elimina un miembro de un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if proyecto.estado == "X":
            response = {"message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if proyecto.estado == "T":
            response = {"message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if not request.user.has_proyecto_perm("add_rolproyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        if request.user.usuario.id == request.data["usuario_id"]:
            response = {"message": "No puede eliminarse a usted mismo"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        miembros = RolProyecto.objects.filter(proyecto=proyecto)
        miembro = get_object_or_404(miembros, usuario=request.data["usuario_id"])
        miembro.delete()
        miembros = RolProyecto.objects.filter(proyecto=proyecto)
        serializer = RolProyectoSerializer(miembros, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def mis_permisos(self, request, pk=None):
        """
        Lista los permisos del usuario de un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_any_proyecto_perm(proyecto):
            return Response([])
        rol = RolProyecto.objects.get(proyecto=proyecto, usuario=request.user.usuario)
        permisos = map(lambda x: x.codename, rol.rol.group.permissions.all())
        return Response(list(permisos))

    @action(detail=True, methods=["POST"])
    def iniciar(self, request, pk=None):
        """
        Inicia un proyecto
        """
        proyectos = Proyecto.objects.all()
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if proyecto.estado == "X":
            response = {"message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if len(Fase.objects.filter(proyecto=proyecto)) == 0:
            response = {"message": "No se puede iniciar un proyecto sin fases"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if proyecto.gerente == None:
            response = {"message": "No se puede iniciar un proyecto sin gerente"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if len(proyecto.nombre) == 0:
            response = {"message": "El proyecto necesita un nombre"}
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        if proyecto.fecha_inicio == None:
            response = {
                "message": "No se puede iniciar un proyecto sin fecha de inicio"
            }
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        proyecto.estado = "I"
        proyecto.save()
        serializer = ProyectoSerializer(proyecto, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def comite(self, request, pk=None):
        """
        Lista los miembros de un comite
        """
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_proyecto_perm("view_rolproyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        serializer = UsuarioSerializer(proyecto.miembros_comite.all(), many=True)
        return Response(serializer.data)

    @comite.mapping.post
    def agregar_miembros_comite(self, request, pk=None):
        """
        Agrega usuarios a Comite
        """

        proyecto = get_object_or_404(Proyecto, pk=pk)
        miembros = request.data

        if len(miembros) == 0:
            response = {"message": "No se paso ningun miembro"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        if proyecto.estado == "X":
            response = {"message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if proyecto.estado == "T":
            response = {"message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if len(miembros) % 2 == 0:
            response = {"message": "Verifique que la cantidad de miembros sea impar"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        # Guardamos los miembros no eliminados y los nuevos
        miembros_no_eliminados = []
        miembros_nuevos = []
        for v in miembros:
            miembro = get_object_or_404(Usuario, pk=v["id"])

            # Verificamos que el usuario sea miembro del proyecto
            if not miembro.user.has_any_proyecto_perm(proyecto):
                response = {"message": "El usuario no es miembro del proyecto"}
                return Response(response, status=status.HTTP_404_NOT_FOUND)

            comite = Comite.objects.filter(miembros=miembro, proyecto=proyecto)
            if len(comite) == 1:  # Si no es miembro eliminado
                miembros_no_eliminados.append(miembro)
            if len(comite) == 0:  # Si es nuevo miembro
                miembros_nuevos.append(miembro)

        # Anulamos los votos de los miembros que ya no pertenecen al comite y agregamos los nuevos miembros al comite
        for miembro in proyecto.miembros_comite.all():

            if not miembro in miembros_no_eliminados and not miembro in miembros_nuevos:

                # Eliminamos su votacion en caso de tener
                comite = Comite.objects.get(miembros=miembro, proyecto=proyecto)
                voto = Voto.objects.filter(solicitud__proyecto=proyecto, comite=comite)
                if len(voto) > 0:
                    voto.delete()

                # Si es miembro eliminado
                proyecto.miembros_comite.remove(miembro)

                # Agregamos al nuevo miembro
        while len(miembros_nuevos) > 0:
            proyecto.miembros_comite.add(miembros_nuevos.pop(0))

        serializer = UsuarioSerializer(proyecto.miembros_comite.all(), many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=["POST"])
    def cancelar(self, request, pk=None):
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_proyecto_perm("delete_proyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        if proyecto.estado != "I":
            response = {"message": "Este proyecto no está iniciado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        proyecto.estado = "X"
        proyecto.save()
        serializer = ProyectoSerializer(proyecto, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=["POST"])
    def terminar(self, request, pk=None):
        proyecto = get_object_or_404(Proyecto, pk=pk)
        if not request.user.has_proyecto_perm("change_proyecto", proyecto):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        if proyecto.estado != "I":
            response = {"message": "Este proyecto no está iniciado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if len(proyecto.fases.filter(estado="A")) > 0:
            response = {"message": "Este proyecto aun tiene fases abiertas"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        proyecto.estado = "T"
        proyecto.save()
        serializer = ProyectoSerializer(proyecto, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=["GET"])
    def solicitudes(self, request, pk=None):
        """
        Listar las solicitudes de un proyecto
        """
        solicitudes = Solicitud.objects.filter(proyecto_id=pk)
        comite = Comite.objects.filter(
            proyecto_id=pk, miembros_id=request.user.usuario.id
        )
        if len(comite) == 0:
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        serializer = SolicitudSerializer(solicitudes, many=True)
        return Response(serializer.data)
