from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from dockerdjango.api.models import Campo
from dockerdjango.api.models import TipoItem
from dockerdjango.api.serializers.campo import CampoSerializer
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404


class CampoViewSet(viewsets.ViewSet):

    def list(self, request):
        """
        Lista todos los campos personalizados
        """
        campos = Campo.objects.all()
        serializer = CampoSerializer(campos, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea un campo personalizado
        """
        campos = Campo.objects.all()
        tipo_item = TipoItem.objects.get(pk=request.data["tipo_item_id"])
        if (tipo_item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        campo = Campo.objects.create(
            nombre=request.data["nombre"],
            tipo_item=tipo_item,
            tipo_dato=request.data["tipo_dato"]

        )
        serializer = CampoSerializer(campo, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene un campo personalizado
        """
        campos = Campo.objects.all()
        campo = get_object_or_404(campos, pk=pk)
        serializer = CampoSerializer(campo, many=False)
        return Response(serializer.data)

    def update(self, request, pk=None):
        """
        Modifca un campo personalizado
        """
        campos = Campo.objects.all()
        campo = get_list_or_404(Campo, pk=pk)
        if (campo[0].tipo_item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (campo[0].tipo_item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        campo = Campo.objects.filter(pk=pk)
        campo.update(**request.data)
        campo[0].save()
        serializer = CampoSerializer(campo[0], many=False)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        """
        Elimina un campo personalizado
        """
        campos = Campo.objects.all()
        campo = get_object_or_404(Campo, pk=pk)
        if (campo.tipo_item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (campo.tipo_item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        campo.delete()
        response = {"message": "Campo Eliminado."}
        return Response(response)
