from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404

from dockerdjango.api.models import LineaBase, Fase, Item
from dockerdjango.api.serializers import LineaBaseSerializer


class LineaBaseViewSet(viewsets.ViewSet):
    def list(self, request):
        """
        Lista las lineas base cerradas
        """
        lineas_base_cerrada = LineaBase.objects.filter(estado="C")
        serializer = LineaBaseSerializer(lineas_base_cerrada, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene una línea base a partir de su id
        """
        linea = get_object_or_404(LineaBase, pk=pk)
        serializer = LineaBaseSerializer(linea, many=False)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea una nueva línea base
        """
        fase = get_object_or_404(Fase, pk=request.data["fase_id"])
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        proyecto = fase.proyecto
        num = len(LineaBase.objects.filter(fase__proyecto=proyecto)) + 1
        linea = LineaBase.objects.create(nombre="LB" + str(num), fase=fase, estado="C")
        if len(request.data["items"]) == 0:
            response = {
                "message": "Debe seleccionar al menos un item para la linea base"
            }
            return Response(response, status=status.HTTP_403_FORBIDDEN)
        for item in request.data["items"]:
            item = get_object_or_404(Item, pk=item["id"])
            if item.fase != fase:
                linea.delete()
                response = {"message": "Un item no pertenece a la fase"}
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            if item.estado != "A":
                linea.delete()
                response = {
                    "message": "Un item no está aprobado o ya pertenece a una linea base"
                }
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            item.estado = "L"
            item.save()
            linea.items.add(item)
        serializer = LineaBaseSerializer(linea, many=False)
        return Response(serializer.data)
