from rest_framework import viewsets
from rest_framework.response import Response
from django.contrib.auth.models import Permission
from dockerdjango.api.serializers.permiso import PermisoSerializer

class PermisoViewSet(viewsets.ViewSet):
    
    def list(self, request):
        """
        Lista los permisos del sistema
        """
        permisos = Permission.objects.filter(content_type__app_label="api")
        serializer = PermisoSerializer(permisos, many=True)
        return Response(serializer.data)
