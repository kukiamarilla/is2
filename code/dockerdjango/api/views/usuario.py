import json
from rest_framework import viewsets,status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth.models import User, Group
from dockerdjango.api.models import Usuario, Rol
from dockerdjango.api.serializers.usuario import UsuarioSerializer
from dockerdjango.api.serializers.rol import RolSerializer
from dockerdjango.api.serializers.group import GroupSerializer
from django.shortcuts import get_object_or_404

class UsuarioViewSet(viewsets.ViewSet):
    def list(self, request):
        """
        Lista todos los usuarios
        """
        if(not request.user.has_perm("api.view_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        serializer = UsuarioSerializer(usuarios, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        """
        Obtiene un usuario
        """
        if(not request.user.has_perm("api.view_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        serializer = UsuarioSerializer(usuario, many=False)
        return Response(serializer.data)

    @action(detail=False, methods=['GET'])
    def me(self, request):
        """
        Obtiene el usuario autenticado
        """
        user = self.request.user
        usuario = Usuario.objects.get(user=user)
        serializer = UsuarioSerializer(usuario, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=['POST'])
    def activar(self, request, pk=None):
        """
        Activa un usuario
        """
        if(not request.user.has_perm("api.change_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        usuario.estado = "A"
        usuario.save()
        serializer = UsuarioSerializer(usuario, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=['POST'])
    def desactivar(self, request, pk=None):
        """
        Desactiva un usuario
        """
        if(not request.user.has_perm("api.change_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        usuario.estado = "I"
        usuario.save()
        serializer = UsuarioSerializer(usuario, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=['GET'])
    def roles(self, request, pk=None):
        """
        Obtiene los roles de un usuario
        """
        if(not request.user.has_perm("api.view_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        user = usuario.user
        roles = map(lambda x: Rol.objects.get(group=x), user.groups.all())
        serializer = RolSerializer(roles, many=True)
        return Response(serializer.data)
    
    @roles.mapping.post
    def agregar_roles(self, request, pk=None):
        """
        Agrega roles a un usuario
        """
        if(not request.user.has_perm("api.change_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        user = usuario.user
        roles = Group.objects.all()
        rol = get_object_or_404(roles, pk=request.data["rol_id"])
        user.groups.add(rol)
        user.save()
        roles = map(lambda x: Rol.objects.get(group=x), user.groups.all())
        serializer = RolSerializer(roles, many=True)
        return Response(serializer.data)
    
    @roles.mapping.delete
    def eliminar_roles(self, request, pk=None):
        """
        Elimina roles de un usuario
        """
        if(not request.user.has_perm("api.change_usuario")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        user = usuario.user
        roles = Group.objects.all()
        rol = get_object_or_404(roles, pk=request.data["rol_id"])
        user.groups.remove(rol)
        user.save()
        roles = map(lambda x: Rol.objects.get(group=x), user.groups.all())
        serializer = RolSerializer(roles, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['GET'])
    def permisos(self, request, pk=None):
        """
        Obtiene permisos un usuario
        """
        if(not request.user.has_perm("api.view_user")):
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=pk)
        user = usuario.user
        permisos = list(user.get_all_permissions())
        return Response(permisos)

    @action(detail=False, methods=['GET'])
    def mis_permisos(self, request, pk=None):
        """
        Obtiene los permisos del usuario autenticado
        """
        permisos = list(request.user.get_all_permissions())
        return Response(permisos)