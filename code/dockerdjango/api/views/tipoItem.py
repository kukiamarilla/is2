from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from dockerdjango.api.models import TipoItem
from dockerdjango.api.models import Fase
from dockerdjango.api.models import Campo
from dockerdjango.api.serializers import TipoItemSerializer
from dockerdjango.api.serializers import CampoSerializer
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404


class TipoItemViewSet(viewsets.ViewSet):

    def list(self, request):
        """
        Lista todos tipo de item
        """
        tipo_items = TipoItem.objects.all()
        serializer = TipoItemSerializer(tipo_items, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea un tipo de item
        Parametros: nombre
        Retorna: TipoItem con un nombre asignado.
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(fases, pk=request.data['fase_id'])
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        tipo_item = TipoItem.objects.create(
            nombre=request.data["nombre"],
            fase=fase
        )
        serializer = TipoItemSerializer(tipo_item, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene un tipo de Item
        """
        tipo_items = TipoItem.objects.all()
        tipo_item = get_object_or_404(TipoItem, pk=pk)
        serializer = TipoItemSerializer(tipo_item, many=False)
        return Response(serializer.data)

    def update(self, request, pk=None):
        """
        Modifica un tipo de Item
        """
        tipo_items = TipoItem.objects.all()
        tipo_item = get_object_or_404(TipoItem, pk=pk)
        if (tipo_item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (tipo_item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        tipo_item.nombre = request.data["nombre"]
        tipo_item.save()
        serializer = TipoItemSerializer(tipo_item, many=False)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        """
        Elimina un tipo de Item
        """
        tipo_items = TipoItem.objects.all()
        tipo_item = get_object_or_404(TipoItem, pk=pk)
        if (tipo_item.fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (tipo_item.fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        tipo_item.delete()
        response = {"message": "Tipo de Item Eliminado."}
        return Response(response)

    @action(detail=True, methods=["GET"])
    def campos(self, request, pk=None):
        """
        Lista los campos de un tipo de Item
        """
        tipo_items = TipoItem.objects.all()
        tipo_item = get_object_or_404(TipoItem, pk=pk)
        campos = Campo.objects.filter(tipo_item_id=tipo_item.id)
        serializer = CampoSerializer(campos, many=True)
        return Response(serializer.data)
