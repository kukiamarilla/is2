from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404

from dockerdjango.api.models import Comite
from dockerdjango.api.models import Proyecto
from dockerdjango.api.models import Usuario

from dockerdjango.api.serializers import ComiteSerializer

class ComiteViewSet(viewsets.ViewSet):
    
    def list(self, request):
        """
        Lista todos los comites de diferentes proyectos
        """
        comites = Comite.objects.all()

        #Agregar permiso

        serializer = ComiteSerializer(comites, many=True)
        return Response(serializer.data)

