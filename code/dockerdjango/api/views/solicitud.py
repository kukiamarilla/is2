from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from dockerdjango.api.models import Solicitud
from dockerdjango.api.models import Usuario
from dockerdjango.api.models import Item
from dockerdjango.api.models import Fase
from dockerdjango.api.models import Proyecto
from dockerdjango.api.models import LineaBase
from dockerdjango.api.models import Comite
from dockerdjango.api.models import Voto
from dockerdjango.api.models import Relacion
from dockerdjango.api.serializers import SolicitudSerializer

import networkx as nx
import json

# Grafos para almacenar items y lineas base
grafo_items_impacto = nx.Graph()
grafo_items = nx.DiGraph()
grafo_lbs = nx.Graph()


class SolicitudViewSet(viewsets.ViewSet):
    def list(self, request):
        """
        Lista todas las solicitudes pendientes
        """
        sldes = Solicitud.objects.all()
        serializer = SolicitudSerializer(sldes, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        solicitud = get_object_or_404(Solicitud, pk=pk)
        proyecto = solicitud.proyecto
        comite = Comite.objects.filter(proyecto=proyecto, miembros=request.user.usuario)
        if len(comite) == 0:
            response = {"message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        serializer = SolicitudSerializer(solicitud, many=False)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea una solicitud de cambio
        """

        # Usuario que solicita el cambio
        usuario = get_object_or_404(Usuario, pk=request.data["usuario_id"])

        # Obtenemos las operaciones a realizar
        operaciones = request.data["operaciones"]

        # Verificamos que la primera operacion sea romper
        operacion = operaciones[0]["operacion"]
        if operacion != "romper":
            response = {"message": "La primera operacion debe ser romper"}
            return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

        # Calculamos proyecto y fase por la linea base dada
        linea_base = get_object_or_404(
            LineaBase, pk=operaciones[0]["payload"]["linea_base"]
        )
        proyecto = linea_base.fase.proyecto
        fase = linea_base.fase

        self.verificar_consistencia_operaciones(operaciones, proyecto)

        # Calculamos el peso total de items
        peso_total = 0.0
        for item_db in Item.objects.filter(fase__proyecto=proyecto):
            if item_db.estado != "C" and item_db.estado != "D":
                peso_total = peso_total + item_db.versiones.get(actual=True).peso

        # Calculamos el impacto de los cambios
        global grafo_items_impacto

        impacto = 0.0
        for item in grafo_items_impacto.nodes:
            impacto = impacto + item.versiones.get(actual=True).peso

        # Creamos la solicitud de cambio
        solicitud = Solicitud.objects.create(
            usuario=usuario,
            operacion=json.dumps(operaciones),
            motivo=request.data["motivo"],
            impacto=(impacto / peso_total) * 100,
            proyecto=proyecto,
        )

        # Serializamos...
        serializer = SolicitudSerializer(solicitud, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["POST"])
    def votar(self, request, pk=None):
        """
        Se aprueba o rechaza una solicitud de cambio por
        """

        solicitud = get_object_or_404(Solicitud, pk=pk)

        # Obtenemos el usuario que vota
        user = request.user
        usuario = Usuario.objects.filter(user=user)[0]

        # Verificamos si el usuario es miembro comite del proyecto
        proyecto = self.obtener_proyecto(json.loads(solicitud.operacion))
        comite = Comite.objects.filter(miembros=usuario, proyecto=proyecto)

        if len(comite) == 0:
            response = {"message": "El usuario no es miembro comite del proyecto"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)

        voto = request.data["votar"]
        response = {"message": "El voto fue en contra"}
        if voto == True:
            response = {"message": "El voto fue a favor"}

        if len(Voto.objects.filter(comite=comite[0], solicitud=solicitud)) == 1:
            response = {"message": "Ya votaste en esta solicitud"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        voto = Voto.objects.create(solicitud=solicitud, comite=comite[0], voto=voto)

        # Contamos la cantidad de votos, para verificar si es la ultima votacion
        cant_votos = len(Voto.objects.filter(comite__proyecto=proyecto))
        cant_miembros_comite = len(proyecto.miembros_comite.all())

        # Si todos los miembros del comite votaron; aplicamos los cambios a la base de datos (en caso de ser consistente)
        if cant_votos == cant_miembros_comite:
            self.verificar_solicitud_aprobacion(pk)

        return Response(response, status=status.HTTP_200_OK)

    @action(detail=True, methods=["GET"])
    def conteo(self, request, pk):
        """
        Retorna la cantidad de votos a favor y en contra. Tambien la cantidad de miembros en comite
        """
        solicitud = get_object_or_404(Solicitud, pk=pk)
        votos = solicitud.votos.all()

        a_favor = 0
        en_contra = 0
        for voto in votos:
            if voto.voto == True:
                a_favor = a_favor + 1
            else:
                en_contra = en_contra + 1

        proyecto = self.obtener_proyecto(json.loads(solicitud.operacion))

        conteo = {
            "a_favor": a_favor,
            "en_contra": en_contra,
            "total": len(proyecto.miembros_comite.all()),
        }

        return Response(conteo, status=status.HTTP_200_OK)

    # Servicios auxiliares

    def verificar_consistencia_operaciones(self, operaciones, proyecto):
        """
        Metodo auxiliar para verificar si los cambios mantienen la consistencia del sistema
        """

        global grafo_items
        global grafo_lbs

        # Si no se enviaron cambios, retorna un 404
        if len(operaciones) == 0:
            response = {"message": "No se envio ningun cambio"}
            return Response(response, status=status.HTTP_404_NOT_FOUND)

        grafo_items = self.crear_grafo_items(proyecto.id)
        grafo_lbs = self.crear_grafo_lbs(proyecto.id)

        # Comprobamos la consistencia de la solicitud
        for i in range(len(operaciones)):

            if operaciones[i]["operacion"] == "aprobar":

                # Obtenemos el item
                item_id = operaciones[i]["payload"]["item"]
                if grafo_items.has_node(item_id) == False:
                    response = {
                        "message": "El item (id="
                        + str(item_id)
                        + ") no fue encontrado para aprobar"
                    }
                    return Response(response, status=status.HTTP_404_NOT_FOUND)

                item = grafo_items.nodes[item_id]["item"]

                # Verificamos que el item este en revision
                if item.estado != "R":
                    print(item.estado)
                    response = {
                        "message": "El item (id="
                        + str(item.id)
                        + ") no se encuentra en revision para aprobar"
                    }
                    return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

                # Aprobamos el item
                item.estado = "AR"

            if operaciones[i]["operacion"] == "desaprobar":
                self.desaprobar_item(
                    grafo_items, grafo_lbs, operaciones[i]["payload"]["item"]
                )

            if operaciones[i]["operacion"] == "romper":

                # Obtenemos la LB a romper
                linea_base_id = operaciones[i]["payload"]["linea_base"]
                if grafo_lbs.has_node(linea_base_id) == False:
                    response = {"message": "No existe la linea base"}
                    return Response(response, status=status.HTTP_404_NOT_FOUND)

                linea_base = grafo_lbs.nodes[linea_base_id]["linea_base"]

                # Si es la primera operacion
                if i == 0:
                    self.romper_linea_base(grafo_lbs, grafo_items, linea_base_id)

                # Si no es la primera operacion
                if i != 0:

                    # Obtenemos los items de la linea base
                    items = linea_base.items.all()

                    # Buscamos si un item tiene al menos algun padre desaprobado
                    padre_desaprobado = False
                    for item in items:
                        for padre_id in grafo_items.predecessors(item.id):
                            padre = grafo_items.nodes[padre_id]["item"]
                            if padre.estado == "C":
                                padre_desaprobado = True
                                break
                        if padre_desaprobado == True:
                            break
                    if padre_desaprobado == False:
                        response = {
                            "message": "Ninguno de los item tienen al menos un padre desaprobado"
                        }
                        return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

                    # Rompemos la linea base
                    self.romper_linea_base(grafo_lbs, grafo_items, linea_base_id)

            if operaciones[i]["operacion"] == "crear":

                # Obtenemos la id de la fase
                fase_id = operaciones[i]["payload"]["fase"]

                # Verificamos si existe la fase
                fase = get_object_or_404(Fase, pk=fase_id)

                # Obtenemos los items
                items = operaciones[i]["payload"]["items"]

                # Verificamos si existe los items
                for item_id in items:
                    if len(Item.objects.filter(pk=item_id)) == 0:
                        response = {"message": "Un item no existe"}
                        return Response(response, status=status.HTTP_404_NOT_FOUND)

                # Verificamos si los items son de la fase correspondiente, si estan aprobados y pertenecen a una linea base rota
                items_ = []
                for item_id in items:
                    item = grafo_items.nodes[item_id]["item"]
                    if item.fase.id != fase_id:
                        response = {
                            "message": "No se puede crear linea base, un item no se encuentra en la misma fase"
                        }
                        return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)
                    if item.estado != "AR":
                        response = {
                            "message": "No se puede crear linea base, el item (id="
                            + str(item_id)
                            + ") no esta aprobado o no estaba en una linea base rota"
                        }
                        return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

                    items_.append(item)

                for item in items_:
                    item.estado = "L"

                # Creamos la linea base virtual
                num = len(LineaBase.objects.filter(fase__proyecto=fase.proyecto)) + 1
                pk = LineaBase.objects.order_by("-id")[0].id + 1
                dict_linea_base = {
                    "fase": fase,
                    "nombre": "LB" + str(num),
                    "estado": "C",
                    "items": items_,
                }

                grafo_lbs.add_node(pk, linea_base=dict_linea_base)

        # Verificamos la consistencia global
        for item_id in grafo_items.nodes:
            item = grafo_items.nodes[item_id]["item"]
            if item.estado == "R":
                response = {
                    "message": "Un item (id="
                    + str(item.id)
                    + ") quedo en revision, no es posible aplicar los cambios"
                }
                return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)
            if item.estado == "AR":
                response = {
                    "message": "El item (id="
                    + str(item.id)
                    + ") aprobado no se encuentra en linea base, no es posible aplicar los cambios"
                }
                return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

    def crear_grafo_items(self, proyecto_id):
        """
        Crea un grafo donde sus nodos son items, y las aristas, relaciones
        """

        # El proyecto que se usa para modelar sus items con la estructura grafo
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Inicializamos un grafo dirigido
        G = nx.DiGraph()

        # Obtenemos las fases del proyecto
        fases = proyecto.fases.all()

        # Guardamos todos los items como nodos del grafo
        for fase in fases:
            for item in fase.items.all():
                G.add_node(item.id, item=item)

        # Creamos la relaciones en el grafo
        for item_hijo_id in G.nodes:
            # Todas las relaciones del item (si tiene)
            item_hijo = G.nodes[item_hijo_id]["item"]
            rel = item_hijo.versiones.get(actual=True).padres.all()

            # Agregamos dos nodos, padre-hijo conectados por una arista dirigida
            for rel_padre in rel:
                G.add_edge(rel_padre.padre.id, item_hijo_id, borrado=False)

        return G

    def crear_grafo_lbs(self, proyecto_id):
        """
        Creamos un grafo donde sus nodos son lineas base
        """

        # El proyecto que se usa para modelar sus lineas base como nodo de un grafo
        proyecto = get_object_or_404(Proyecto, pk=proyecto_id)

        # Inicializamos un grafo dirigido
        G = nx.DiGraph()

        # Obtenemos las fases del proyecto
        fases = proyecto.fases.all()

        # Guardamos todas las lineas base como nodos
        for fase in fases:
            for linea_base in fase.lineas_base.all():
                G.add_node(linea_base.id, linea_base=linea_base)
        return G

    def romper_linea_base(self, grafo_lbs, grafo_items, linea_base_id):
        """
        Rompe la linea base por si sola, y lo que implica. Los items y sus hijos quedan en revision
        """

        # Obtenemos la linea base
        linea_base = grafo_lbs.nodes[linea_base_id]["linea_base"]

        # Verificamos que no este ya rota
        if linea_base.estado != "R":

            # Rompemos la linea base
            linea_base.estado = "R"

            # Obtenemos los items de esa linea base
            items = linea_base.items.all()

            # Ponemos los items de la linea base en revision
            for item in items:
                grafo_items.nodes[item.id]["item"].estado = "R"

    def desaprobar_item(self, grafo_items, grafo_lbs, item_id):
        """
        Metodo recursivo para desaprobar un item
        """

        # Verificamos si existe el item
        if grafo_items.has_node(item_id) == False:
            response = {"message": "Un item no fue encontrado"}
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        item = grafo_items.nodes[item_id]["item"]

        # Verificamos si el item esta en revision
        item_revision = False
        if item.estado == "R":
            item_revision = True

        # Buscamos si el item tiene al menos un padre desaprobado y no esta en linea base
        padres = grafo_items.predecessors(item.id)
        padre_desaprobado = False
        for padre_id in padres:
            item_padre = grafo_items.nodes[padre_id]["item"]
            if item_padre.estado == "C" and item.estado != "L":
                padre_desaprobado = True
                break
        if item_revision == False and padre_desaprobado == False:
            response = {
                "message": "El item no tiene ningun padre desaprobado para poder desaprobar o no esta en revision"
            }
            return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

        # Desaprobamos el item
        item.estado = "C"

        # Guardamos el item que ocasiona impacto
        global grafo_items_impacto
        grafo_items_impacto.add_node(item)

        # Rompemos las lineas bases, y eliminamos las relaciones del item y sus hijos
        for hijo_id in list(grafo_items.successors(item.id)):

            hijo = grafo_items.nodes[hijo_id]["item"]
            grafo_items.add_edge(item.id, hijo.id, borrado=True)

            for linea_base in hijo.lineas_base.filter(estado="C"):
                self.romper_linea_base(grafo_lbs, grafo_items, linea_base.id)

            # Desaprobamos su hijo
            self.desaprobar_item(grafo_items, grafo_lbs, hijo_id)

    def obtener_proyecto(self, operaciones):
        """
        Obtiene el proyecto mediante la primera operacion
        """

        operacion = operaciones[0]["operacion"]
        if operacion == "romper":
            linea_base = get_object_or_404(
                LineaBase, pk=operaciones[0]["payload"]["linea_base"]
            )
            proyecto = linea_base.fase.proyecto
            return proyecto
        else:
            response = {"message": "La primera operacion debe ser romper"}
            return Response(response, status=status.HTTP_406_NOT_ACCEPTABLE)

    def verificar_solicitud_aprobacion(self, pk):
        """
        Verifica una solicitud de cambio por mayoria de votos; siempre y cuando sea consistente la solicitud
        """

        solicitud = get_object_or_404(Solicitud, pk=pk)

        if solicitud.estado == "R":
            response = {
                "message": "La solicitud fue rechazada previamente, no es posible aprobar"
            }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        if solicitud.estado == "A":
            response = {"message": "La solicitud ya fue aprobada"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        # Obtenemos las operaciones, y el proyecto
        operaciones = json.loads(solicitud.operacion)
        proyecto = self.obtener_proyecto(operaciones)

        # Verificamos virtualmente los cambios
        self.verificar_consistencia_operaciones(operaciones, proyecto)

        # Verificamos los votos de los miembros del comite
        votos = solicitud.votos.all()

        a_favor = 0
        en_contra = 0
        for voto in votos:
            if voto.voto == True:
                a_favor = a_favor + 1
            else:
                en_contra = en_contra + 1

        if a_favor > en_contra:
            solicitud.estado = "A"
            solicitud.save()
        if en_contra > a_favor:
            solicitud.estado = "R"
            solicitud.save()

        # Aplicamos los cambios a la base de datos si fue aprobada
        if solicitud.estado == "R":
            response = {"message": "La solicitud fue rechazada"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        for item_id in grafo_items.nodes:
            item_bd = Item.objects.get(pk=item_id)
            item_virtual = grafo_items.nodes[item_id]["item"]

            item_bd.estado = item_virtual.estado
            item_bd.save()

            for hijo_id in grafo_items.successors(item_id):
                if grafo_items.edges[item_id, hijo_id]["borrado"] == True:
                    relacion = Relacion.objects.filter(padre=item_bd)[0]
                    relacion.delete()

        for lb_id in grafo_lbs.nodes:
            lb_virtual = grafo_lbs.nodes[lb_id]["linea_base"]
            if len(LineaBase.objects.filter(pk=lb_id)) == 0:
                fase = lb_virtual["fase"]
                nombre = lb_virtual["nombre"]
                estado = lb_virtual["estado"]
                linea_base = LineaBase.objects.create(
                    fase=fase, nombre=nombre, estado=estado
                )
                for item in lb_virtual["items"]:
                    linea_base.items.add(item)
                linea_base.save()
            else:
                lb_bd = LineaBase.objects.get(pk=lb_id)

                lb_bd.estado = lb_virtual.estado
                lb_bd.save()

        response = {"message": "Solicitud de cambio aplicada"}
        return Response(response, status=status.HTTP_200_OK)