from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from dockerdjango.api.models import Fase
from dockerdjango.api.models import Proyecto
from dockerdjango.api.models import Rol
from dockerdjango.api.models import RolFase
from dockerdjango.api.models import Usuario
from dockerdjango.api.models import TipoItem
from dockerdjango.api.models import Campo
from dockerdjango.api.models import Item
from dockerdjango.api.models import Version
from dockerdjango.api.models import ValorPersonalizado
from dockerdjango.api.models import Relacion
from dockerdjango.api.serializers.fase import FaseSerializer
from dockerdjango.api.serializers.rolFase import RolFaseSerializer
from dockerdjango.api.serializers.tipoItem import TipoItemSerializer
from dockerdjango.api.serializers import ItemSerializer
from dockerdjango.api.serializers import LineaBaseSerializer
from dockerdjango.api.helpers.fases import has_path_to
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404
from datetime import date
import pyrebase
from django.conf import settings
from django.db.models import Q
import copy


class FaseViewSet(viewsets.ViewSet):
    def list(self, request):
        """
        Lista todas las fases
        """
        fases = Fase.objects.all().order_by("orden")
        serializer = FaseSerializer(fases, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea una fase
        """
        f = Fase.objects.all()
        proyecto = Proyecto.objects.get(pk=request.data["proyecto_id"])
        if (proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        fase = Fase.objects.create(
            nombre=request.data["nombre"],
            estado="A",
            proyecto=proyecto,
            orden=proyecto.fases.all().count() + 1,
        )
        qa_fase = get_object_or_404(Usuario, pk=request.data["usuario_id"])
        rol = Rol.objects.get(tipo="F", group__name="QA")
        RolFase.objects.create(fase=fase, usuario=qa_fase, rol=rol)

        serializer = FaseSerializer(fase, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene una fase
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(fases, pk=pk)
        serializer = FaseSerializer(fase, many=False)
        return Response(serializer.data)

    def update(self, request, pk=None):
        """
        Modifica una fase
        """
        fases = Fase.objects.all()
        fase = get_list_or_404(Fase, pk=pk)
        fase = Fase.objects.filter(pk=pk)
        if (fase[0].proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase[0].proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        fase.update(
            nombre=request.data["nombre"],
        )
        fase[0].save()
        serializer = FaseSerializer(fase[0], many=False)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        """
        Elimina una fase
        """
        fases = Fase.objects.all().order_by("orden")
        fase = get_object_or_404(Fase, pk=pk)

        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        pos = Fase.objects.get(pk=pk).orden-1
        fases[pos].delete()

        while pos < fases.count():
            fase_temp = fases[pos]
            fase_temp.orden = pos + 1
            fase_temp.save()
            pos += 1
        response = {"message": "Fase Eliminada."}
        return Response(response)

    @action(detail=True, methods=["PUT"])
    def intercambiar(self, request, pk=None):
        """
        Intercambia el orden de dos fases
        """
        fases = Fase.objects.all()
        fase_origen = get_object_or_404(Fase, pk=pk)
        if (fase_origen.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase_origen.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        fase_destino = Fase.objects.filter(pk=request.data["id"])[0]
        pos_origen = fase_origen.orden
        fase_origen.orden = fase_destino.orden
        fase_origen.save()

        fase_destino.orden = pos_origen
        fase_destino.save()

        serializer = FaseSerializer(fases.order_by("orden"), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def miembros(self, request, pk=None):
        """
        Lista los miembros de una fase
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(Fase, pk=pk)
        if not request.user.has_fase_perm(
            "view_rolfase", fase
        ) and not request.user.has_proyecto_perm("add_rolfase", fase.proyecto):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        miembros = RolFase.objects.filter(fase=fase)
        serializer = RolFaseSerializer(miembros, many=True)
        return Response(serializer.data)

    @miembros.mapping.post
    def agregar_miembro(self, request, pk=None):
        """
        Agrega un miembro a un fase
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(Fase, pk=pk)
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if(not request.user.has_fase_perm("add_rolfase", fase) and not request.user.has_proyecto_perm("add_rolfase", fase.proyecto)):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=request.data["rol_id"])
        if rol.tipo != "F":
            response = {"message": "Este no es un rol de fase"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        usuarios = Usuario.objects.all()
        usuario = get_object_or_404(usuarios, pk=request.data["usuario_id"])
        if usuario.user.has_any_fase_perm(fase):
            response = {"message": "Este usuario ya es miembro del fase"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        miembro = RolFase.objects.create(fase=fase, usuario=usuario, rol=rol)
        miembros = RolFase.objects.filter(fase=fase)
        serializer = RolFaseSerializer(miembros, many=True)
        return Response(serializer.data)

    @miembros.mapping.delete
    def eliminar_miembro(self, request, pk=None):
        """
        Elimina un miembro una fase
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(Fase, pk=pk)
        if(fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if(not request.user.has_fase_perm("add_rolfase", fase) and not request.user.has_proyecto_perm("add_rolfase", fase.proyecto)):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        miembros = RolFase.objects.filter(fase=fase)
        miembro = get_object_or_404(
            miembros, usuario=request.data["usuario_id"])
        miembro.delete()
        miembros = RolFase.objects.filter(fase=fase)
        serializer = RolFaseSerializer(miembros, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def mis_permisos(self, request, pk=None):
        """
        Lista los permisos del usuario de una fase
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(Fase, pk=pk)
        if not request.user.has_any_fase_perm(fase):
            return Response([])
        rol = RolFase.objects.get(fase=fase, usuario=request.user.usuario)
        permisos = map(lambda x: x.codename, rol.rol.group.permissions.all())
        return Response(list(permisos))

    @action(detail=True, methods=["GET"])
    def tipo_items(self, request, pk=None):
        """
        Lista los tipos de items de una fase
        """
        fases = Fase.objects.all()
        fase = get_object_or_404(Fase, pk=pk)
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        tipo_items = fase.tipoitem_set.all()
        serializer = TipoItemSerializer(tipo_items, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["POST"])
    def importar(self, request, pk=None):
        """
        Asigna un tipo de item a una fase
        """
        fase = get_object_or_404(Fase, pk=pk)
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        tipo_item = get_object_or_404(
            TipoItem, pk=request.data["tipo_item_id"])

        n = len(tipo_item.campos.all())
        campos = []
        for i in range(0, n):
            campo = Campo.objects.create(
                nombre=tipo_item.campos.all()[i].nombre,
                tipo_item=tipo_item,
                tipo_dato=tipo_item.campos.all()[i].tipo_dato,
            )
            campos.append(campo)

        plantilla = TipoItem.objects.create(nombre=tipo_item.nombre, fase=fase)
        plantilla.campos.set(campos)
        plantilla.save()
        serializer = TipoItemSerializer(plantilla, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def items(self, request, pk=None):
        """
        Lista todos los items de la fase
        """

        fase = get_object_or_404(Fase, pk=pk)

        serializer = ItemSerializer(fase.items, many=True)

        return Response(serializer.data)

    @items.mapping.post
    def add_item(self, request, pk=None):
        """
        Agrega un item a la fase
        """
        fase = get_object_or_404(Fase, pk=pk)
        plantilla = get_object_or_404(
            TipoItem, pk=request.POST["tipo_item_id"], fase=fase
        )

        item = Item.objects.create(
            fecha_creado=date.today(), fase=fase, tipo=plantilla)

        archivo_path_on_cloud = ""

        if len(request.FILES) != 0:
            # Recibimos el archivo
            archivo = request.FILES["file"]

            # Recibimos la config de firebase
            firebase = pyrebase.initialize_app(settings.FIREBASE_CLIENT_CONFIG)
            storage = firebase.storage()

            # Ruta del archivo en firebase (cloud)
            archivo_path_on_cloud = "items/" + \
                str(item.id) + "/files/" + archivo.name

            # Almacenamos el archivo en la nube
            storage.child(archivo_path_on_cloud).put(archivo)

        primera_version = Version.objects.create(
            numero=1,
            nombre=request.POST["nombre"],
            peso=request.POST["peso"],
            archivo=archivo_path_on_cloud,
            item=item,
            actual=True,
        )

        # Guardamos los valores de cada campo
        campos = item.tipo.campos.all()
        for campo in campos:
            ValorPersonalizado.objects.create(
                version=primera_version, campo=campo, valor=request.POST[campo.nombre]
            )

        # Guardamos los padres
        padres = request.POST.getlist("padres", [])
        for padre in padres:
            padre = Item.objects.get(pk=padre)
            if padre.estado != "L":
                item.delete()
                response = {"message": "Un padre no está en linea base"}
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            if (
                padre.fase.orden != item.fase.orden
                and padre.fase.orden != item.fase.orden - 1
            ):
                item.delete()
                response = {
                    "message": "Un padre no esta en la fase actual o en la anterior"
                }
                return Response(response, status=status.HTTP_403_FORBIDDEN)
            Relacion.objects.create(padre=padre, hijo=primera_version)
        serializer = ItemSerializer(item, many=False)
        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def lineas_base(self, request, pk=None):
        """
        Lista todas las lineas base de la fase
        """

        fase = get_object_or_404(Fase, pk=pk)

        serializer = LineaBaseSerializer(fase.lineas_base, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=["GET"])
    def posibles_padres(self, request, pk=None):

        fase = get_object_or_404(Fase, pk=pk)

        padres = Item.objects.filter(
            Q(fase__proyecto=fase.proyecto)
            & (Q(fase__orden=fase.orden) | Q(fase__orden=fase.orden - 1))
            & Q(estado="L")
        )

        serializer = ItemSerializer(padres, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["POST"])
    def cerrar(self, request, pk=None):
        fase = get_object_or_404(Fase, pk=pk)
        if (fase.proyecto.estado == "X"):
            response = {
                "message": "Este proyecto está cancelado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.proyecto.estado == "T"):
            response = {
                "message": "Este proyecto está terminado"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (len(fase.items.filter(estado__in=["C", "A"])) != 0):
            response = {
                "message": "Algunos items aún no están en línea base"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (len(fase.items.exclude(estado="D")) == 0):
            response = {
                "message": "La fase no tiene items"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        if (fase.orden == 1):
            fase.estado = "C"
            fase.save()
            serializer = FaseSerializer(fase, many=False)
            return Response(serializer.data)
        fase_anterior = Fase.objects.get(
            proyecto=fase.proyecto, orden=fase.orden - 1)
        if (fase_anterior.estado == "A"):
            response = {
                "message": "La fase anterior aún no está cerrada"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        for item in fase_anterior.items.all():
            if (not has_path_to(item, fase)):
                response = {
                    "message": "Un item de la fase anterior no tiene camino a esta fase"}
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        fase.estado = "C"
        fase.save()
        serializer = FaseSerializer(fase, many=False)
        return Response(serializer.data)
