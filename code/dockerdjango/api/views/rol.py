from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth.models import Group, Permission
from dockerdjango.api.models import Rol
from dockerdjango.api.serializers.rol import RolSerializer
from dockerdjango.api.serializers.permiso import PermisoSerializer
from django.shortcuts import get_object_or_404


class RolViewSet(viewsets.ViewSet):

    def list(self, request):
        """ 
        Lista todos los usuarios
        """
        if(not request.user.has_perm("api.view_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        tipo = request.GET.get("tipo", "")
        roles = Rol.objects.filter(tipo__startswith=tipo)
        serializer = RolSerializer(roles, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        Crea un rol
        """
        if(not request.user.has_perm("api.add_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        group = Group.objects.create(name=request.data["name"])
        rol = Rol.objects.create(tipo=request.data["tipo"], group=group)
        serializer = RolSerializer(rol, many=False)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Obtiene un rol
        """
        if(not request.user.has_perm("api.view_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=pk)
        serializer = RolSerializer(rol, many=False)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        """
        Elimina un rol
        """
        if(not request.user.has_perm("api.delete_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=pk)
        rol.group.delete()
        rol.delete()
        response = {"message": "Rol Eliminado."}
        return Response(response)

    @action(detail=True, methods=["GET"])
    def permisos(self, request, pk=None):
        """
        Lista los permisos de un rol
        """
        if(not request.user.has_perm("api.view_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=pk)
        permisos = rol.group.permissions.all()
        serializer = PermisoSerializer(permisos, many=True)
        return Response(serializer.data)

    @permisos.mapping.post
    def agregar_permiso(self, request, pk=None):
        """
        Agrega un permiso a un rol
        """
        if(not request.user.has_perm("api.change_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=pk)
        permisos = Permission.objects.all()
        permiso = get_object_or_404(permisos, pk=request.data["permiso_id"])
        rol.group.permissions.add(permiso)
        serializer = RolSerializer(rol, many=False)
        return Response(serializer.data)

    @permisos.mapping.delete
    def eliminar_permiso(self, request, pk=None):
        """
        Elimina un permiso de un rol
        """
        if(not request.user.has_perm("api.change_rol")):
            response = {
                "message": "No tiene permiso para realizar esta acción"}
            return Response(response, status=status.HTTP_401_UNAUTHORIZED)
        roles = Rol.objects.all()
        rol = get_object_or_404(roles, pk=pk)
        permisos = Permission.objects.all()
        permiso = get_object_or_404(permisos, pk=request.data["permiso_id"])
        rol.group.permissions.remove(permiso)
        serializer = RolSerializer(rol, many=False)
        return Response(serializer.data)
