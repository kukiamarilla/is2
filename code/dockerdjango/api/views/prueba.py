from rest_framework import viewsets
from rest_framework.response import Response
from django.contrib.auth.models import User
class PruebaViewSet(viewsets.ViewSet):
    def list(self, request):
        return Response(request.user.first_name)