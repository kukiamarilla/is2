from dockerdjango.api.models.rolProyecto import RolProyecto
from dockerdjango.api.models.rolFase import RolFase

def has_any_proyecto_perm(self, proyecto):
    """
    Verfica si el usuario tiene algun permiso en el proyecto
    """
    relation = RolProyecto.objects.filter(user=self, proyecto=proyecto)
    return len(relation) == 1

def has_any_fase_perm(self, fase):
    """
    Verfica si el usuario tiene algun permiso en la fase
    """
    relation = RolFase.objects.filter(user=self, fase=fase)
    return len(relation) == 1

def has_proyecto_perm(self, permission, proyecto):
    """
    Verfica si el usuario tiene el permiso especificado en el proyecto
    """
    relation = RolProyecto.objects.filter(user=self, proyecto=proyecto)
    if(not self.has_any_proyecto_perm()): 
        return False
    return len(relation[0].group.permissions.filter(codename=permission)) == 1

def has_fase_perm(self, permission, false):
    """
    Verfica si el usuario tiene el permiso especificado en la fase
    """
    relation = RolFase.objects.filter(user=self, false=false)
    if(not self.has_any_fase_perm()): 
        return False
    return len(relation[0].group.permissions.filter(codename=permission)) == 1

