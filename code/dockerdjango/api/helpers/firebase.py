import pyrebase
import os
from django.conf import settings


def get_file_from_firebase(path):
    basename = os.path.basename(path)
    firebase = pyrebase.initialize_app(settings.FIREBASE_CLIENT_CONFIG)
    storage = firebase.storage()
    storage.child(path).download(basename)
    f = open(basename, 'rb')
    file_data = f.read()
    os.remove(basename)
    return file_data
