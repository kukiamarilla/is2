import smtplib
from django.conf import settings


def sendmail(to_addr, msg, subject):
    from_addr = settings.MAIL_SENDER_EMAIL
    s = smtplib.SMTP("smtp.gmail.com", 587)
    s.starttls()
    s.login(settings.MAIL_SENDER_EMAIL, settings.MAIL_SENDER_PASSWORD)
    s.sendmail(from_addr, to_addr, msg)
