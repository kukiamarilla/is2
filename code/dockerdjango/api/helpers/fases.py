def has_path_to(item, fase):
    if (item.estado == "D"):
        return False
    if (item.fase_id == fase.id):
        return True
    for relacion in item.hijos.all():
        if (not relacion.hijo.actual):
            continue
        hijo = relacion.hijo.item
        if (has_path_to(hijo, fase)):
            return True
    return False
