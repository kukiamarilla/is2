"""dockerdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from dockerdjango.api.views.usuario import UsuarioViewSet
from dockerdjango.api.views.proyecto import ProyectoViewSet
from dockerdjango.api.views.fase import FaseViewSet
from dockerdjango.api.views.permiso import PermisoViewSet
from dockerdjango.api.views.rol import RolViewSet
from dockerdjango.api.views import TipoItemViewSet
from dockerdjango.api.views import CampoViewSet
from dockerdjango.api.views import ComiteViewSet
from dockerdjango.api.views import ItemViewSet
from dockerdjango.api.views import LineaBaseViewSet
from dockerdjango.api.views import SolicitudViewSet

router = routers.DefaultRouter()
router.register("usuarios", UsuarioViewSet, basename="usuarios")
router.register("proyectos", ProyectoViewSet, basename="proyectos")
router.register("fases", FaseViewSet, basename="fases")
router.register("permisos", PermisoViewSet, basename="permisos")
router.register("roles", RolViewSet, basename="roles")
router.register("tipo-item", TipoItemViewSet, basename="tipo-item")
router.register("campos", CampoViewSet, basename="campos")
router.register("items", ItemViewSet, basename="items")
router.register("lineas-base", LineaBaseViewSet, basename="lineas-base")
router.register("solicitud", SolicitudViewSet, basename="solicitud")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(router.urls)),
]
